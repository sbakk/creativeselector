# -*- coding: utf-8 -*-
from django.contrib import admin
from creativeselector.models import *
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from django.db.models import Q
from django import forms
from django.utils import simplejson
from django.utils.safestring import mark_safe
from tagging.models import Tag
from tagging.forms import TagField

class AutoCompleteTagInput(forms.TextInput):
    class Media:
        css = {
            'all': ('/media/css/jquery.autocomplete.css',)
        }
        js = (
            '/media/js/jquery-1.4.2.min.js',
            '/media/js/jquery.bgiframe.min.js',
            '/media/js/jquery.ajaxQueue.js',
            '/media/js/jquery.autocomplete.pack.js'
        )

    def render(self, name, value, attrs=None):
        output = super(AutoCompleteTagInput, self).render(name, value, attrs)
        page_tags = Tag.objects.all()
        tag_list = simplejson.dumps([tag.name for tag in page_tags],
                                    ensure_ascii=False)
        return output + mark_safe(u'''<script type="text/javascript">
            jQuery("#id_%s").autocomplete(%s, {
                width: 150,
                max: 10,
                highlight: false,
                multiple: true,
                multipleSeparator: ", ",
                scroll: true,
                scrollHeight: 300,
                matchContains: true,
                autoFill: true,
            });
            </script>''' % (name, tag_list))
            

class BlogEntryAdminModelForm(forms.ModelForm):
    tagss = TagField(widget=AutoCompleteTagInput(), required=False)

    class Meta:
        model = BlogEntry     

class StaffAdmin(UserAdmin):
  pass
  
class StaticpageAdmin(admin.ModelAdmin):
  model = Staticpage
  save_on_top = True
  list_per_page = 20
  readonly_fields = ('link',)
  
class ExampleAdmin(admin.ModelAdmin):
  model = Example
  save_on_top = True
  list_per_page = 20
  readonly_fields = ('link',)  
  

class BlogEntryAdmin(admin.ModelAdmin):
  form = BlogEntryAdminModelForm
  model = BlogEntry
  save_on_top = True
  list_per_page = 20
  readonly_fields = ('link',)  
  list_filter = ('created', )
  list_display = ('title', 'tagss', 'created', 'published')  

##ez új  
class BlogCommentAdmin(admin.ModelAdmin):
  model = BlogComment
  save_on_top = True
  list_per_page = 50
  list_display = ('comment', 'created', 'user', 'blog')

class BlackWordsAdmin(admin.ModelAdmin):
  model = BlackWords
  save_on_top = True
  list_per_page = 50
  list_display = ('word')

class TenderImageStandaloneAdmin(admin.ModelAdmin):
  model = TenderImage
  save_on_top = True
  list_per_page = 20  
  list_display = ('image', 'file')
  list_filter = ('image',) 
  
class SiteMetaAdmin(admin.ModelAdmin):
  model = SiteMeta
  save_on_top = True
  list_per_page = 20
  fields = ('link', 'title', 'keywords', 'description', )
  readonly_fields = ('link',)  
  
class TenderImageAdmin(admin.TabularInline):
  model = TenderImage
  extra = 0
  max_num = 100

  
class TenderSupporterAdmin(admin.TabularInline):
  model = TenderSupporter
  extra = 0
  max_num = 100
  
class TenderSupporterStandaloneAdmin(admin.ModelAdmin):
  model = TenderSupporter
  save_on_top = True
  list_per_page = 20  
  list_display = ('user', 'idea')
  list_filter = ('user', 'idea',) 
  
class TenderAdmin(admin.ModelAdmin):
  model = Tender
  save_on_top = True
  list_per_page = 20
  list_display = ('short_title', 'link', 'created', 'category', 'full_money',  'status')
  list_filter = ('category','owner',)
  
  inlines = [
              TenderImageAdmin,
            ]
  fieldsets = (
          (None, {
              'fields': ('category', 'owner', 'created', 'from_when', 'serial', 'status', 'approved', 'crowd_no', 'full_money',  'visitors', 'expiry', 'link', 'tagss', )
          }),
          ('Leírás', {
              'classes': ('collapse',),
              'fields': ('short_title','introduce','for_what', 'main_pic', 'budget', 'why_sup',  'faceurl', 'location', 'tel_no', 'video_url')
          }),
      )
  readonly_fields = ('serial', 'visitors', 'created')

class TenderPresentAdmin(admin.ModelAdmin):
    model = TenderPresent
    save_on_top = True
    list_per_page = 20
    list_display = ('tender', 'min_prize', 'detail')
    list_filter = ('tender',)

class FacebookProfileAdmin(admin.ModelAdmin):
  model = FacebookProfile
  save_on_top = True
  list_per_page = 100

class UserProfileAdmin(admin.ModelAdmin):
  model = UserNoFaceProfile
  save_on_top = True
  list_per_page = 100  
  
class NeccCategoryAdmin(admin.ModelAdmin):
  model = NeccCategory
  save_on_top = True
  list_per_page = 20
  #readonly_fields = ('link','created')

class TenderUpdateAdmin(admin.ModelAdmin):
  model = TenderUpdate
  save_on_top = True
  list_per_page = 50
  list_display = ('idea', 'content', 'link',)
  #ordering = ('-created')
  
class SupporterCompanyAdmin(admin.ModelAdmin):
  model = SupporterCompany
  save_on_top = True
  list_per_page = 20
  


    
  
admin.site.register(BlogEntry, BlogEntryAdmin)
admin.site.register(SiteMeta, SiteMetaAdmin)
admin.site.register(Staticpage, StaticpageAdmin)
admin.site.register(Tender, TenderAdmin)
admin.site.register(TenderPresent, TenderPresentAdmin)
admin.site.register(BlogComment, BlogCommentAdmin)
admin.site.register(TenderImage, TenderImageStandaloneAdmin)
admin.site.register(TenderSupporter, TenderSupporterStandaloneAdmin)
admin.site.register(BlackWords, BlackWordsAdmin)
admin.site.register(FacebookProfile, FacebookProfileAdmin)
admin.site.register(UserNoFaceProfile, UserProfileAdmin)
admin.site.register(NeccCategory, NeccCategoryAdmin)
admin.site.register(TenderUpdate, TenderUpdateAdmin)
admin.site.register(SupporterCompany, SupporterCompanyAdmin)

