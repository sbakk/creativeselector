# -*- coding: utf-8 -*-
from django import forms
from creativeselector.models import *
from django.db import models
from django.template.loader import render_to_string
from django.forms.widgets import Select, MultiWidget, DateInput, TextInput
from time import strftime
from tagging.forms import TagField
from tagging_autocomplete.widgets import TagAutocomplete
from time import strptime, strftime
from django.forms import fields

class JqSplitDateTimeWidget(MultiWidget):

    def __init__(self, attrs=None, date_format=None, time_format=None):
        date_class = attrs['date_class']
        time_class = attrs['time_class']
        del attrs['date_class']
        del attrs['time_class']

        date_attrs = attrs.copy()
        date_attrs['class'] = date_class
        hour_attrs = attrs.copy()
        hour_attrs['class'] = time_class
        hour_attrs['value'] = '12'
        min_attrs = attrs.copy()
        min_attrs['class'] = time_class
        min_attrs['value'] = '00'
        ampm_attrs = attrs.copy()
        ampm_attrs['class'] = time_class
        ampm_attrs['value'] = 'AM'

        widgets = (DateInput(attrs=date_attrs, format=date_format),
                   forms.HiddenInput(attrs=hour_attrs), forms.HiddenInput(attrs=min_attrs),
                   forms.HiddenInput(attrs=ampm_attrs))

        super(JqSplitDateTimeWidget, self).__init__(widgets, attrs)

    def decompress(self, value):
        if value:
            d = strftime("%Y-%m-%d", value.timetuple())
            hour = strftime("%I", value.timetuple())
            minute = strftime("%M", value.timetuple())
            meridian = strftime("%p", value.timetuple())
            return (d, hour, minute, meridian)
        else:
            return (None, None, None, None)

    def format_output(self, rendered_widgets):
        """
        Given a list of rendered widgets (as strings), it inserts an HTML
        linebreak between them.

        Returns a Unicode string representing the HTML for the whole lot.
        """
        return " %s  %s %s %s" % (rendered_widgets[0], rendered_widgets[1],
                                                rendered_widgets[2], rendered_widgets[3])

    class Media:
        css = { }
        js = ("js/jqsplitdatetime.js", "js/jquery-1.6.2.min.js", "js/jquery-ui-1.8.16.custom.min.js",)

class JqSplitDateTimeField(fields.MultiValueField):
    widget = JqSplitDateTimeWidget

    def __init__(self, *args, **kwargs):
        """
        Have to pass a list of field types to the constructor, else we
        won't get any data to our compress method.
        """
        all_fields = (
            fields.CharField(max_length=10),
            fields.CharField(max_length=2),
            fields.CharField(max_length=2),
            fields.ChoiceField(choices=[('AM','AM'),('PM','PM')])
            )
        super(JqSplitDateTimeField, self).__init__(all_fields, *args, **kwargs)

    def compress(self, data_list):
        """
        Takes the values from the MultiWidget and passes them as a
        list to this function. This function needs to compress the
        list into a single object to save.
        """
        if data_list:
            if not (data_list[0] and data_list[1] and data_list[2] and data_list[3]):
                raise forms.ValidationError("Field is missing data.")
            input_time = strptime("%s %s %s"%(data_list[1], data_list[2], data_list[3]), "%I %M %p")
            datetime_string = "%s %s" % (data_list[0], strftime('%H:%M', input_time))
            print "Datetime: %s"%datetime_string
            return datetime_string
        return None


class TenderForm(forms.ModelForm):
    tagss = TagField(widget=TagAutocomplete(), required=False)
    from_when = JqSplitDateTimeField(widget=JqSplitDateTimeWidget(attrs={'date_class':'datepicker','time_class':'timepicker'}), required=False)
    expiry = JqSplitDateTimeField(widget=JqSplitDateTimeWidget(attrs={'date_class':'datepicker','time_class':'timepicker'}), required=False)
    class Meta:
      model = Tender
      exclude = ('created', 'serial', 'link', 'status', 'visitors', )

class TenderpresentForm(forms.ModelForm):
    class Meta:
        model = TenderPresent
        exclude =('tender')

class TenderupdateForm(forms.ModelForm):
    class Meta:
        model = TenderUpdate
        exclude =('created','link','idea')

class TendersupportForm(forms.ModelForm):
    class Meta:
        model = TenderSupporter
        exclude =('idea','date','visibility','pres')

class NewTenderAdminForm(forms.ModelForm):
    class Meta:
        model = NewTenderAdmin
        exclude =('id', 'proj',)


class LoginForm(forms.Form):
    email = forms.CharField(label="E-mail:")
    password = forms.CharField(label="Jelszó:", widget=forms.PasswordInput(render_value=False))
    #forget = forms.HiddenInput(label="Elfeljetett", required=False)

class ForgetForm(forms.Form):
    email = forms.CharField(label="E-mail:")
    password = forms.CharField(label="Jelszó:", widget=forms.PasswordInput(render_value=False))
    password2 = forms.CharField(label="Jelszó:", widget=forms.PasswordInput(render_value=False))

class ValidateForm(forms.Form):
    mail_text = forms.CharField(widget=forms.Textarea, label="Miért nem jó a projekt:")

class BlogCommentForm(forms.ModelForm):
    class Meta:
        model = BlogComment
        exclude = ('blog', 'user', 'created', 'slug')

class TenderCommentForm(forms.ModelForm):
    class Meta:
        model = TenderComment
        exclude = ('idea', 'user', 'created', 'slug')

class UserNoFaceRegisterForm(forms.ModelForm):
    class Meta:
        model = UserNoFaceProfile
        exclude = ('user', 'user_id')
