import re 
from django.utils.encoding import force_unicode

from django import template
register = template.Library()

@register.filter
def replace ( string, args ): 
    search  = args.split(args[0])[1]
    replace = args.split(args[0])[2]

    return re.sub( search, replace, string )
    
@register.filter   
def strip(value):
    value = re.sub(r'\n\s+', '\n', force_unicode(value)) # Replace all leading spaces at the beginning of a line!
    value = re.sub(r'>\s+<', '><', force_unicode(value))
    return value.strip()