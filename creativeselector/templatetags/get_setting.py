from django import template
from django.views.debug import get_safe_settings
register = template.Library()

class SettingNode(template.Node):
    def __init__(self, varname, setting):
        self.setting = setting
        self.varname = varname

    def render(self, context):
        context[self.varname] = self.setting
        return ""
        
#@register.tag
def get_setting(parser, token):
    """
    Retrieve a setting from the current settings file.  The optional as clause can control
    the variable to be added to the context, otherwise the setting asked for witll be added to
    the context.  For example::
        
        {% get_setting MEDIA_URL [as varname] %}
        
    If the setting does not exist in the current settings file, ``None`` will be returned.  The
    following example will return a bulleted list of installed apps::
    
        {% get_setting INSTALLED_APPS %}
        <ul>
        {% for app in INSTALLED_APPS %}
            <li>{{ app }}</li>
        {% endfor %}
        </ul>   
    """   
    bits = token.contents.split()
    if len(bits) == 2:
        varname = bits[1]
    elif len(bits) == 4:
        varname = bits[3]
    else:
        raise template.TemplateSyntaxError, "'%s' takes either one or three arguments." % bits[0]
    try:
        settings = get_safe_settings()
        retsetting = settings[bits[1]]
    except KeyError:
        retsetting = None
    return SettingNode(varname, retsetting)

get_setting = register.tag('get_setting', get_setting)