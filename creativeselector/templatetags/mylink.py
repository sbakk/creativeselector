import logging
from django import template
register = template.Library()

class LinkNode(template.Node):
    def __init__(self, url):
        self.url = url

    def render(self, context):
        formatted_url = "/%s" % self.url
        return formatted_url

def mylink(parser, token):
    try:
        tag_name, format_string = token.split_contents()
    except ValueError:
        raise template.TemplateSyntaxError, "%r tag requires a single argument" % token.contents.split()[0]
    if not (format_string[0] == format_string[-1] and format_string[0] in ('"', "'")):
        raise template.TemplateSyntaxError, "%r tag's argument should be in quotes" % tag_name
   
    return LinkNode(format_string[1:-1])
    
mylink = register.tag('mylink', mylink)
