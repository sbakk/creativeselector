from django.forms import ChoiceField, FileField

from django import template
register = template.Library()

@register.filter(name='format_number')
def format_number(value, format): 
	""" 
	Returns the displayed value for this BoundField, as rendered in widgets. 
	""" 
	value = format % value
	return value