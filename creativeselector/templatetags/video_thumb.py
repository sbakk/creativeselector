from django.template.defaultfilters import stringfilter
from django import template
import re

register = template.Library()

youtube_regex = re.compile("((http|https)://)?(www\.)?(youtube\.com/watch\?v=)(?P<video_id>[A-Za-z0-9\-=_]{11})([^\s|\<]*)(&kepszam=)(?P<pic_no>[0-9]{1})?", re.VERBOSE)
vimeo_regex = re.compile("(http://)?(www\.)?(vimeo\.com/)(?P<video_id>[0-9]{8})([^\s|\<]*)",  re.VERBOSE)

youtube_thumbnail = """
  <img width="195" height="145" src="http://img.youtube.com/vi/\g<video_id>/\g<pic_no>.jpg"/>
"""

@register.filter
@stringfilter
def video_thumb(url):
    result = youtube_regex.sub(youtube_thumbnail, url)
    return result    
    
video_thumb.is_safe = True
