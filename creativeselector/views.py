# -*- coding: utf-8 -*-
import captcha
import logging
from datetime import datetime, timedelta
from django.http import HttpResponseRedirect, Http404, HttpResponsePermanentRedirect, HttpResponse
from django.shortcuts import get_object_or_404
from django.views.generic.simple import direct_to_template
from creativeselector.forms import *
from creativeselector.models import *
from creativeselector.util import send_mail, Paginator, CibPayTrans, slugify
from creativeselector.maillist import getcsv_list
from django.views.decorators.cache import cache_control
from django.views.decorators.vary import vary_on_cookie
from django.shortcuts import get_object_or_404
from django.contrib.auth import authenticate, login, logout

from filetransfers.api import prepare_upload
from django.core.urlresolvers import reverse
from django_facebook.view_decorators import _facebook
from django_facebook.facebook import GraphAPI
from functools import partial
from django.forms.formsets import formset_factory, BaseFormSet
from django.forms.models import modelformset_factory
from tagging.models import Tag, TaggedItem

import feedparser
from datetime import datetime
from django.template.defaultfilters import removetags, truncatewords_html, stringfilter, striptags
import re

import urllib, urllib2

from google.appengine.api import urlfetch
from django.conf import settings

captcha_public_key = settings.RECAPTCHA_PUB_KEY

#########################
#Mobile brwosers
#########################
reg_b = re.compile(r"android.+mobile|avantgo|bada\\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\\/|plucker|pocket|psp|symbian|treo|up\\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino", re.I|re.M)
reg_v = re.compile(r"1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\\-(n|u)|c55\\/|capi|ccwa|cdm\\-|cell|chtm|cldc|cmd\\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\\-s|devi|dica|dmob|do(c|p)o|ds(12|\\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\\-|_)|g1 u|g560|gene|gf\\-5|g\\-mo|go(\\.w|od)|gr(ad|un)|haie|hcit|hd\\-(m|p|t)|hei\\-|hi(pt|ta)|hp( i|ip)|hs\\-c|ht(c(\\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\\-(20|go|ma)|i230|iac( |\\-|\\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\\/)|klon|kpt |kwc\\-|kyo(c|k)|le(no|xi)|lg( g|\\/(k|l|u)|50|54|e\\-|e\\/|\\-[a-w])|libw|lynx|m1\\-w|m3ga|m50\\/|ma(te|ui|xo)|mc(01|21|ca)|m\\-cr|me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\\-2|po(ck|rt|se)|prox|psio|pt\\-g|qa\\-a|qc(07|12|21|32|60|\\-[2-7]|i\\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\\-|oo|p\\-)|sdk\\/|se(c(\\-|0|1)|47|mc|nd|ri)|sgh\\-|shar|sie(\\-|m)|sk\\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\\-|v\\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\\-|tdg\\-|tel(i|m)|tim\\-|t\\-mo|to(pl|sh)|ts(70|m\\-|m3|m5)|tx\\-9|up(\\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|xda(\\-|2|g)|yas\\-|your|zeto|zte\\-", re.I|re.M)

def is_mobile(request):
    if request.META.has_key('HTTP_USER_AGENT'):
        user_agent = request.META['HTTP_USER_AGENT']
        b = reg_b.search(user_agent)
        v = reg_v.search(user_agent[0:4])
        if b or v:
            return True
        else:
            return False


def check_user(request):
    #if request.user.is_authenticated():
     # request.facebook = partial(_facebook, request)
      #logged_in = request.facebook().is_authenticated()
      #if not logged_in:
       # logout(request)
    return request.user.is_authenticated()

def is_admin(request):
    if request.user.is_authenticated() and request.user.is_staff:
        return True
    return False

def is_super(request):
    if request.user.is_authenticated() and request.user.is_superuser:
        return True
    return False

def is_pass_ok(request):
    #if 'main_pass' in request.session and request.session['main_pass']:
        #return True
    return True

    
def django_root_url(request):
    secure = ""
    if (request.is_secure()):
      secure = "s"
    
    return u"http%s://%s%s" % (secure, request.get_host(), request.path)

###################################
# GENERIC MESSAGE HELPER
###################################
def show_message(request, title, message):
  return render_response(request, 'message.html', {'title': title, 'message': message})

###################################
# GETTING METAS TO SEO
###################################
def get_meta(key):
  meta = None
  try:
    meta = SiteMeta.objects.get(link = key)
  except:
    pass
  return meta

###################################
# HELPER METHODS
###################################
def format_title(meta, variables_dict):
  if variables_dict.has_key('headtitle'):
      if variables_dict['headtitle'] == u"Creative Selector":
          return u".:: CreativeSelector ::."
      return u".:: CreativeSelector - %s ::." % variables_dict['headtitle']
  elif meta and not variables_dict.has_key('headtitle'):
      if meta.title == u"Creative Selector":
          return u".:: CreativeSelector ::."
      return u".:: CreativeSelector - %s ::." % meta.title
  elif not meta and not variables_dict.has_key('headtitle'):
    return u".:: CreativeSelector ::."
    
###################################
# Facebook sharing hack
###################################
from django.template.defaultfilters import removetags
from django.template import loader, RequestContext
from django.http import HttpResponse, HttpResponseRedirect, HttpResponsePermanentRedirect, HttpResponseGone

def cs_direct_to_template(request, temp, extra_context=None, mimetype=None, **kwargs):
    """
    Render a given template with any extra URL parameters in the context as
    ``{{ params }}``.
    """
    if extra_context is None: extra_context = {}
    dictionary = {'params': kwargs}
    for key, value in extra_context.items():
        if callable(value):
            dictionary[key] = value()
        else:
            dictionary[key] = value
    c = RequestContext(request, dictionary)
    t = loader.get_template(temp)
    if 'is_english' in request.session and request.session['is_english']:
        from django import template
        try:
            tmp='%sen.html' % temp[:-5]
            t = loader.get_template(tmp)
        except template.TemplateDoesNotExist:
            pass
    
    rendered = t.render(c);
    
    #logging.error("AGENT = %s" % request.META['HTTP_USER_AGENT'].lower())
    
    if 'HTTP_USER_AGENT' in request.META and "facebook" in request.META['HTTP_USER_AGENT'].lower():
      rendered =  removetags(rendered, "img link");
      rendered = rendered.replace("<body>", "<body><img src='http://www.creativeselector.hu/media/images/logo.jpg' />")
    
    return HttpResponse(rendered, mimetype=mimetype)


@cache_control(public=True)
def render_response(request, template, variables_dict, meta=None):
    if is_pass_ok(request):
        variables_dict['MEDIA_PREFIX'] = settings.MEDIA_PREFIX
        
        # Meta tags
        if meta:
          if not variables_dict.has_key('description'):
            variables_dict['description'] = meta.description
          if not variables_dict.has_key('keywords'):
            variables_dict['keywords'] = meta.keywords
          if not variables_dict.has_key('image'):
            variables_dict['image'] = "http://www.creativeselector.hu/media/images/logo.jpg"
        if variables_dict.has_key('image') and variables_dict['image'] == None:
          variables_dict['image'] = "http://www.creativeselector.hu/media/images/logo.jpg"
        
        variables_dict['headtitle'] = format_title(meta, variables_dict)
        
        # Blog entries for the blog sidebox
        side_blogs = BlogEntry.objects.filter(published = True).order_by('-created')[0:5]
        side_blogss=[]
        for blog in side_blogs:
            img=get_img(blog.content)
            blog.content=get_first_paragh(blog.content)
            side_blogss.append([img,blog])
        variables_dict['side_blogs'] = side_blogss
        
            
        # full url for sharing
        variables_dict['full_url'] = django_root_url(request)  
        
        variables_dict['rss_source'] = settings.RSS_SOURCE
        
        # next variable for facebook redirect
        if not variables_dict.has_key('next'):
          variables_dict['next'] = request.path
        

        if not variables_dict.has_key('logged_in'):
            variables_dict['logged_in'] =  check_user(request)

        if not variables_dict.has_key('fb_user'):
            try:
                fbuser=FacebookProfile.objects.get(user=request.user)
            except:
                fbuser=None
            if fbuser:
                variables_dict['fb_user'] =  True
        
        categorys=None
        try:
            categorys = NeccCategory.objects.all().order_by('link')
            variables_dict['categories'] = categorys
        except:
            pass
        if 'categ' in request.session:
            variables_dict['cate'] = request.session['categ']
        supportcomp=None
        try:
            supportcomp = SupporterCompany.objects.all()
            variables_dict['supportcomp'] = supportcomp
        except:
            pass
        if 'is_english' in request.session and request.session['is_english']:
            variables_dict['is_english'] = True

        # REFERER FIX FOR IE8
        request.session['HTTP_REFERER'] = django_root_url(request)

        #detect mac
        if request.META['HTTP_USER_AGENT'].find('Mac')>=0 and request.META['HTTP_USER_AGENT'].find('Firefox')>=0:
            variables_dict['is_mac'] = True
        
        #if not settings.DEBUG:
        #  try:
        #    return direct_to_template(request, template, variables_dict)
        #  except:
        #    raise Http404
        #else:
        return cs_direct_to_template(request, template, variables_dict)
    else:
        return cs_direct_to_template(request, 'login.html', )
      
###################################
# INDEX
###################################
def index(request):
    if 'categ' in request.session:
        del request.session['categ']
    proj = MostTenders.objects.all().order_by('weight')[0:12]
    projdayvid=[]
    num = MostTenders.objects.all().count()
    right=False
    if num>6:
        right=True
    from datetime import datetime
    for pr in proj:
        left=pr.tender.expiry-datetime.now()
        lf=0
        orak=False
        if left.days==0:
            lf=left.seconds/3600
            orak=True
        elif left.days<0:
            lf=False
        else:
            lf=left.days
        try:
            sup = TenderSupporter.objects.all().filter(idea = pr.tender)
        except:
            sup=None
        now = 0
        percent = 0
        for i in sup:
            if i.anum:
                now+=i.money
        need=float(now)/float(pr.tender.full_money)
        percent=float(need)*100.0
        img=None        
        if need>=1.0:
            need=1
        projdayvid.append([pr.tender, lf, orak, now, need, round(percent,2)])
    return render_response(request, 'base.html', {
        'projs':projdayvid,
        'right': right,
        'is_admin': is_admin(request),
        'index': range(1, 18)
    }, get_meta("/"))

#def get_video_id(vid):
#    video_re = vid.split("http://www.youtube.com/watch?v=")
#    return video_re[1]
    
                         

####################################
# USER REG AND OTHER STUFFS
####################################
def userreg(request):
    if request.method == 'POST':
        form = UserNoFaceRegisterForm(request.POST, request.FILES)
        if form.is_valid():
            try:
                usr = User.objects.get(email__exact=form.cleaned_data['mail'])
            except:
                usr=None
            if not usr:
                user = form.save(commit=False)
                user.set_password(form.cleaned_data['password'])
                user.save()
                send_mail(u"admin@creativeselector.hu", user.mail, u"Üdv. a Creative Selectoron" , "email/new_user_reg_html", "email/new_user_reg_html", { 'username' : u"%s %s" % (user.last_name, user.first_name), 'usr':user,'url': u"http://%s/activate/%s" % (request.get_host(),user.id )})
                #send_mail(u"admin@creativeselector.hu", "opitzer@gmail.com", u"új ember a cs-en" , "email/new_user_reg", "email/new_user_reg_html", { 'username' : u"%s %s" % (user.last_name, user.first_name),'url': u"url"})
                return show_message(request, "Sikeres regisztráció", "<p>Köszönjük a regisztrációd!</p><blockquote><p>Hamarosan egy emailt kapsz tőlünk, amivel véglegesíteni tudod a regisztrációd.  </p></blockquote><p>Üdvözlünk a Creative Selector közösségben!</p>")
            else:
                return show_message(request, "Sikertelen Regisztráció", "Sajnos ezzel az e-mail(%s) címmel már regisztráltak" % (str(form.cleaned_data['mail'])) )
        else:
            logging.error(form.errors)
            return show_message(request, "Sikertelen Regisztráció", "Hiba történt!<br> Kérlek értesísd az adminisztrátort")
    else:
        try:
            tos = Staticpage.objects.get(link = u"felhasznalasi_feltetelek")
        except:
            tos = None
        return render_response(request, 'user/reg.html', {
            'tos' : tos,
            }, get_meta("/profil"))

def activate_user(request, user_id):
    usr = UserNoFaceProfile.objects.get(id=user_id)
    user = authenticate(email=usr.mail, password=usr.password2)
    if user is not None:
        user.is_active=True
        user.save()
        try:
            ne = NameExport.objects.get(email__exact=user.email)
        except:
            ne = None
        if not ne:
            ne = NameExport(userid=user.id, username=user.username, first_name=user.first_name, last_name=user.last_name, email=user.email)
            ne.save()
        login(request, user)
        return HttpResponseRedirect("/profilom")
    else:
        return show_message(request, "Valami hiba lépett fel", "Kérlek értesísd az adminisztrátort")

def forget_pass(request, num):
    try:
        usr = User.objects.get(password__exact=num)
        try:
            noface = UserNoFaceProfile.objects.get(mail=usr.email)
        except:
            noface=None
        if request.method == 'POST':
            form = ForgetForm(request.POST)
            if form.is_valid():                
                email = form.cleaned_data['email']
                password = form.cleaned_data['password']
                if noface:
                    noface.set_password(password)
                    noface.save(False)
                    usr.password=noface.password
                else:
                    import random
                    algo = 'sha1'
                    salt = get_hexdigest(algo, str(random.random()), str(random.random()))[:5]
                    hsh = get_hexdigest(algo, salt, password)
                    usr.password='%s$%s$%s' % (algo, salt, hsh)
                usr.is_active=True
                usr.save()
                return show_message(request, "Sikeresen elmentve", "Kérlek lépj be az új jelszavaddal")
        else:
            return render_response(request, 'user/forget.html', {
                'usr':usr,
            }, get_meta("/profil"))
    except:
        return show_message(request, "Valami hiba lépett fel", "Valami nem stimmel a megadott kóddal, írj légyszíves az adminisztrátoroknak")
               
        
def passed_page(request):
    if request.method == 'POST':
        login=request.POST['email']
        passw=request.POST['password']
        if login == 'Creative' and passw=='CS_starter01':
            request.session['main_pass']=True
    return HttpResponseRedirect("/")

def user_login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data['email']
            password = form.cleaned_data['password']
            forget = None
            if 'forgot' in request.POST:
                forget = request.POST['forgot']
            if forget == 'True':
                try:
                    usr1 = User.objects.get(email__exact=email)
                except:
                    return show_message(request, "Nem létező e-mail", "Sajnos a rendeszrünkben nem létezik az emailcímed")
                from random import randrange
                rand = randrange(0, 100000, 2)
                usr1.password = rand
                usr1.save()
                send_mail(u"admin@creativeselector.hu", usr1.email, u"Elfelejtett jelszó" , "email/forget_pass", "email/forget_pass", { 'username' : u"%s %s" % (usr1.last_name, usr1.first_name),'url': u"http://%s/forget_pass/%s" % (request.get_host(),rand )})
                #send_mail(u"admin@creativeselector.hu", u"opitzer@gmail.com", u"Elfelejtett jelszó" , "email/forget_pass", "email/forget_pass", { 'username' : u"%s %s" % (usr1.last_name, usr1.first_name),'url': u"http://%s/forget_pass/%s" % (request.get_host(),rand )})
                return show_message(request, "Új jelszó kérés", "e-mailben kapott linkre kattintva tudsz új jelszavat megadni")
                                    
            usr = None
            try:
                usr = UserNoFaceProfile.objects.get(mail=email)
                if usr.check_password(password):
                    user = authenticate(email=email, password=password)                            
                    if user is not None:
                        if user.is_active:
                            login(request, user)
                        else:
                            return show_message(request, "Aktiválás", "Előbb aktiváld magad")
                    else:
                        return show_message(request, "Valami hiba lépett fel", u"Kérlek értesísd az adminisztrátort" )
                    return HttpResponseRedirect(request.session.get('HTTP_REFERER', None))
                else:
                    return show_message(request, "Hibás jelszó", "Hibás jelszót + (" + email + "/" + password + ") adott meg " + password)               
            except:
                user = authenticate(email=email, password=password)                            
                if user is not None:
                    if user.is_active:
                        login(request, user)
                    else:
                        return show_message(request, "Aktiválás", "előbb aktiváld magad")
                    return HttpResponseRedirect(request.session.get('HTTP_REFERER', None))
                else:
                    return show_message(request, u"Nem stimmel az email címed vagy a jelszavad", u"<p><strong>Nem stimmel az email címed(%s) vagy a jelszavad(%s) amit az imént megadtál.</strong></p><p><strong>Kérjük, pr&#243;báld meg újra a belépést az oldalra!</strong></p>" % (email, password))
        else:
            return show_message(request, "Ooopsz", "Valamit kihagytál, kérlek próbáld <a href=\"javascript:window.history.back();\">újra</a>!")
    return HttpResponseRedirect("/")

def user_logout(request):
    logout(request)
    return HttpResponseRedirect("/")

def profile(request):
    own_tenders = None
    projs_num=0
    supported_tenders = None
    supps_num=0
    #st_end = None
    sup_ideas=[]
    supps=[]
    projs=[]
    facebook=False
    nf=None
    old_tenders=None
    if check_user(request):
        try:
            own_tenders = Tender.objects.filter(owner = request.user, status=2)
            projs_num = Tender.objects.filter(owner = request.user, status=2).count()
            old_tenders = Tender.objects.filter(owner = request.user, status=3)
            supported_tenders = TenderSupporter.objects.all().filter(user = request.user)
        except:
            pass
        for sup in supported_tenders:
            if sup.idea not in supps and (sup.idea.approved or sup.idea.status==3):
                sup.idea.approved=True
                sup_ideas.append(sup.idea)
                supps.append(sup.idea)
                supps_num+=1
        for pr in own_tenders:
            projs.append([True, pr])
        for pr in old_tenders:
            pr.approved=True
        try:
            fbuser=FacebookProfile.objects.get(user=request.user)
            facebook=True
        except:
            try:
                nf =  UserNoFaceProfile.objects.get(user=request.user)
            except:
                pass
    return render_response(request, 'user/profil_in.html', {
                                  'projs': projs,
                                  'is_face' : facebook,
                                  'nf' : nf,
                                  'projs_num': projs_num,
                                  'supps': sup_ideas,
                                  'supps_num': supps_num,
                                  'old_projs': old_tenders,
                                  'logged_in': check_user(request),
                                  'no_stuff': True,
                            }, get_meta('/profil'))  

def get_user_profile(request,username):
    usr=User.objects.get(username=username)
    projs=None
    projs_num=0
    supps=None
    supps_num=0
    facebook=False
    prs=[]
    try:
        projs=Tender.objects.all().filter(owner = usr)
        for pr in projs:
            if pr.approved or pr.status==3:
                pr.approved=True
                prs.append(pr)
                projs_num+=1
    except:
        pass
    try:
        supps=TenderSupporter.objects.all().filter(user = usr)
        supppp=[]
        for sup in supps:
            if sup.idea not in supppp and (sup.idea.approved or sup.idea.status==3):
                if sup.visibility==1 or sup.visibility==2:
                    sup.idea.approved=True
                    supppp.append(sup.idea)
        #supps_num=TenderSupporter.objects.all().filter(user = usr).count()
    except:
        pass
    try:
        fbuser=FacebookProfile.objects.get(user=usr)
        facebook=True
    except:
        pass
    return render_response(request, 'user/profil_out.html', {
                'usr':usr,
                'is_face' : facebook,
                'projs' : prs,
                'projs_num' : projs_num,
                'supps' : supppp,
                'supps_num' : len(supppp),
                'no_stuff': True,
            }, get_meta("/profil"))

def unsubscribe(request):
    if request.method == 'POST':
        if 'email' in request.POST:
            email = request.POST['email']
            #send_mail(u"admin@creativeselector.hu", "info@creativeselector.hu", u"Email leiratkozás" , "email/unsub", "email/unsub", { 'email' : email})
            try:
                ne = NameExport.objects.get(email__exact=email)
                ne.delete()
            except:
                pass
            return show_message(request, "Sikeresen leíratkoztál", u"Sikeresen leíratkoztál a hírlevelünkről" )
        else:
            return HttpResponseRedirect("/")
    else:
        return render_response(request, 'user/unsub.html',   {}, get_meta("/profil"))
    
###################################
# DETAILED TENDER
###################################    
def detailed_tender(request):

  section_0 = Staticpage.objects.get(link = u"pályázati_felhívás")
  section_1 = Staticpage.objects.get(link = u"pályázható_kategóriák")
  section_2 = Staticpage.objects.get(link = u"hogyan_küldheted_be_az_ötleteidet")
  section_3 = Staticpage.objects.get(link = u"kiválasztás_szempontjai")
  section_4 = Staticpage.objects.get(link = u"támogatás")
  section_5 = Staticpage.objects.get(link = u"nyeremények")
  section_6 = Staticpage.objects.get(link = u"a_pályázatok_benyújtásának_helye_módja_és_ideje")
  
  return render_response(request, 'detailed_tender.html', {
       'section_0': section_0,
       'section_1': section_1,
       'section_2': section_2,
       'section_3': section_3,
       'section_4': section_4,
       'section_5': section_5,
       'section_6': section_6,
   }, get_meta("/"))
  

###################################
# STATIC PAGE 
###################################
def render_direct(request, slug):
    try:
      staticpage = Staticpage.objects.get(link = slug)      
      return render_response(request, 'staticpage.html', {'staticpage':staticpage,}, get_meta("/%s" % slug))
    except:
      try:
        return render_response(request, '%s.html' % slug, {}, get_meta("/%s" % slug))
      except:
        return render_response(request, '404.html', {}, get_meta("/%s" % slug))

def to_english(request):
    if not 'is_english' in request.session or request.session['is_english']!=True:
        request.session['is_english'] = True
    else:
        request.session['is_english'] = False
    return HttpResponseRedirect(request.session.get('HTTP_REFERER', None))
    
###################################
# ADMIN things
###################################
def admin_projs(request):
    if is_admin(request):
        proj = Tender.objects.all().filter(status__gte=2).order_by('status')
        projdayvid=[]
        fullmoney=0
        all_sup=0
        avg_days=0
        avg_perc=0
        all_money_now=0
        our_money=0
        from datetime import datetime
        for pr in proj:
            if pr.approved or pr.status==3:
                left=pr.expiry-datetime.now()
                time=pr.expiry-pr.from_when
                avg_days+=int(time.days)
                ourmoney=0
                try:
                    sup = TenderSupporter.objects.all().filter(idea = pr)
                    all_sup+=len(sup)
                except:
                    sup=None
                now = 0
                percent = 0
                for i in sup:
                    if i.anum:
                        now+=i.money
                need=float(now)/float(pr.full_money)
                percent=float(need)*100.0
                avg_perc+=percent
                fullmoney+=pr.full_money
                all_money_now+=now
                if need>1.0:
                    ourmoney=float(now)*0.08
                    now1=now*0.92
                else:
                    ourmoney=float(now)*0.12
                    now1=now*0.88
                our_money+=ourmoney            
                projdayvid.append([pr, left.days, time.days, sup, now, now1, ourmoney, round(percent,2)])
        avg_fullmoney=fullmoney/len(proj)
        avg_sup=all_sup/len(proj)
        avg_days=avg_days/len(proj)
        avg_perc=avg_perc/len(proj)
        avg_money_now=all_money_now/len(proj)
        return render_response(request, 'admin_proj.html', {
            'projs':projdayvid,
            'fullmoney':fullmoney,
            'all_sup':all_sup,
            'avg_days':avg_days,
            'avg_perc':round(avg_perc,2),
            'all_money_now':all_money_now,
            'avg_fullmoney':avg_fullmoney,
            'avg_sup':avg_sup,
            'our_money':our_money,
            'avg_money_now':avg_money_now,
            }, get_meta("/"))
    else:
        return HttpResponseRedirect("/")

def admin_end_projs(request):
    if is_admin(request):
        proj = Tender.objects.all().filter(status=3)
        projdayvid=[]
        fullmoney=0
        all_sup=0
        avg_days=0
        avg_perc=0
        all_money_now=0
        our_money=0
        from datetime import datetime
        for pr in proj:
            left=pr.expiry-datetime.now()
            time=pr.expiry-pr.from_when
            avg_days+=int(time.days)
            ourmoney=0
            try:
                sup = TenderSupporter.objects.all().filter(idea = pr)
                bud = TenderBudget.objects.get(tender = pr)
                all_sup+=len(sup)
            except:
                sup=None
                bud= None
            now = 0
            percent = 0
            for i in sup:
                if i.anum:
                    now+=i.money
            need=float(now)/float(pr.full_money)
            percent=float(need)*100.0
            avg_perc+=percent
            fullmoney+=pr.full_money
            all_money_now+=now
            if need>1.0:
                ourmoney=float(now)*0.08
                now1=now*0.92
            else:
                ourmoney=float(now)*0.12
                now1=now*0.88
            our_money+=ourmoney            
            projdayvid.append([pr, left.days, time.days, sup, bud, now, now1, ourmoney, round(percent,2)])
        avg_fullmoney=fullmoney/len(proj)
        avg_sup=all_sup/len(proj)
        avg_days=avg_days/len(proj)
        avg_perc=avg_perc/len(proj)
        avg_money_now=all_money_now/len(proj)
        return render_response(request, 'admin_end_proj.html', {
            'projs':projdayvid,
            'fullmoney':fullmoney,
            'all_sup':all_sup,
            'avg_days':avg_days,
            'avg_perc':round(avg_perc,2),
            'all_money_now':all_money_now,
            'avg_fullmoney':avg_fullmoney,
            'avg_sup':avg_sup,
            'our_money':our_money,
            'avg_money_now':avg_money_now,
            }, get_meta("/"))
    else:
        return HttpResponseRedirect("/")

def admin_sups(request):
    if is_admin(request):
        sups = TenderSupporter.objects.all().order_by('date')
        sups_=[]
        #sups_names=[]
        fullmoney=0
        #i=0
        for sup in sups:
            if sup.anum:
                mn=sup.money*0.98
                fullmoney+=sup.money
                sups_.append([mn,sup])
        avg_money=fullmoney/len(sups)
        return render_response(request, 'admin_sups.html', {
            'sups':sups_,
            'fullmoney':fullmoney,
            'avg_money':avg_money,
            }, get_meta("/"))
    else:
        return HttpResponseRedirect("/")

def admin_budget(request):
    if is_admin(request):
        proj = Tender.objects.all().filter(status__gte=2).order_by('status')
        projdayvid=[]
        fullmoney=0
        all_sup=0        
        avg_perc=0
        all_money_now=0
        our_money=0
        from datetime import datetime
        for pr in proj:
            if pr.approved or pr.status==3:
                left=pr.expiry-datetime.now()
                try:
                    sup = TenderSupporter.objects.all().filter(idea = pr)
                    bud = TenderBudget.objects.get(tender = pr)
                    all_sup+=len(sup)
                except:
                    sup=None
                    bud=None
                now = 0
                percent = 0
                for i in sup:
                    if i.anum:
                        now+=i.money
                need=float(now)/float(pr.full_money)
                percent=float(need)*100.0
                avg_perc+=percent
                fullmoney+=pr.full_money
                if need>1.0:
                    ourmoney=float(now)*0.08
                    now1=now*0.92
                else:
                    ourmoney=float(now)*0.12
                    now1=now*0.88
                our_money+=ourmoney   
                all_money_now+=now1
                projdayvid.append([pr, bud, left.days, sup, now, now1, ourmoney, round(percent,2)])
        avg_fullmoney=fullmoney/len(proj)
        avg_sup=all_sup/len(proj)
        avg_perc=avg_perc/len(proj)
        avg_money_now=all_money_now/len(proj)
        return render_response(request, 'admin_budget.html', {
            'projs':projdayvid,
            'fullmoney':fullmoney,
            'all_sup':all_sup,            
            'avg_perc':round(avg_perc,2),
            'all_money_now':all_money_now,
            'our_money':our_money,
            'avg_fullmoney':avg_fullmoney,
            'avg_sup':avg_sup,
            'avg_money_now':avg_money_now,
            }, get_meta("/"))
    else:
        return HttpResponseRedirect("/")

def admin_new_proj(request):
    if is_super(request):
        #projs = Tender.objects.all().filter(approved=False)
        new_pr = []        
        if request.method == "POST":
            projs = Tender.objects.all().filter(approved=False)
            for pr in projs:
                if pr.status!=3:
                    ne=None
                    try:
                        ne = NewTenderAdmin.objects.get(proj=pr)
                    except:
                        ne = NewTenderAdmin(proj=pr)
                    if '%s-activ_status'%pr.pk in request.POST:
                        ne.activ_status=request.POST['%s-activ_status'%pr.pk]
                    if '%s-how_stand'%pr.pk in request.POST:
                        ne.how_stand=request.POST['%s-how_stand'%pr.pk]
                    if '%s-comment'%pr.pk in request.POST:
                        ne.comment=request.POST['%s-comment'%pr.pk]
                    ne.save()
                    if '%s-delete'%pr.pk in request.POST and request.POST['%s-delete'%pr.pk]=='True':
                        ne.delete()
                        pr.delete()
        projs = Tender.objects.all().filter(approved=False)
        for pr in projs:
            if pr.status!=3:
                try:
                    new = NewTenderAdmin.objects.get(proj=pr)
                except:
                    new = None
                new_pr.append([pr,new])
        return render_response(request, 'admin_newprojs.html', {
            'projs':new_pr,
            'is_admin': is_admin(request),
            }, get_meta("/"))
    else:
        return HttpResponseRedirect("/")

def admin_list_proj(request,slug):
    if is_admin(request):
        proj = get_object_or_404(Tender, link = slug)
        sups = TenderSupporter.objects.all().filter(idea = proj)
        bud = TenderBudget.objects.get(tender = proj)
        allsups=0
        supps=[]
        for sp in sups:
            if sp.anum:
                allsups+=sp.money
                supps.append(sp)
        if sups:
            avgsups=allsups/len(sups)
        else:
            avgsups = 0
        from datetime import datetime
        left=proj.expiry-datetime.now()
        lf=left.days
        if lf<0:
            lf=0        
        need=float(allsups)/float(proj.full_money)
        percent=float(need)*100.0
        if need>1.0:
            ourmoney=float(allsups)*0.08
            ownermoney=float(allsups)*0.92
        else:
            ourmoney=float(allsups)*0.12
            ownermoney=float(allsups)*0.88
        return render_response(request, 'admin_proj_detail.html', {
            'proj':proj,
            'sups': supps,
            'bud':bud,
            'allsups':allsups,
            'left': lf,
            'percent':round(percent,2),
            'avgsups': avgsups,
            'ourmoney' : ourmoney,
            'ownermoney' : ownermoney,
            }, get_meta("/"))
    else:
        return HttpResponseRedirect("/")

def mosttender_admin(request):
    if is_admin(request):
        if request.method == "POST":
            mosts=MostTenders.objects.all().order_by('weight')
            for mt in mosts:
                #if mt.pk in request.POST:
                mt.weight=request.POST["%s" % mt.pk]
                mt.save()
            return HttpResponseRedirect(reverse("creativeselector.views.mosttender_admin"))
        else:
            mosts=MostTenders.objects.all().order_by('weight')
            i=1
            for mt in mosts:
                if not mt.weight:
                    mt.weight=i
                    mt.save()
                i+=1
            return render_response(request, 'admin_proj_mosts.html', {
                'mosts':mosts,
                }, get_meta("/"))
    else:
        return HttpResponseRedirect("/")

def admin_names(request):
    if is_admin(request):
        #names = NameExport.objects.all()
        names=None
        return render_response(request, 'admin_proj_namelist.html', {
                'names':names,
                }, get_meta("/"))
    else:
        return HttpResponseRedirect("/")

import csv
def names_to_excel(request):
    if is_admin(request):
        from datetime import datetime
        names = NameExport.objects.all()
        now=datetime.now()
        response = HttpResponse(mimetype='text/csv')                                     
        response['Content-Disposition'] = 'attachment; filename=cs_users_%s_%s_%s.csv' %(now.year, now.month, now.day)
        book = csv.writer(response, delimiter=';')
        book.writerow([u"#", u"Vezeteknev", u"Keresztnev", u"Email"])
        i=1
        for nm in names:
            book.writerow([str(i), nm.last_name.encode('ISO-8859-2'), nm.first_name.encode('ISO-8859-2'), nm.email.encode('ISO-8859-2')])
            i+=1
        return response
    else:
        return HttpResponseRedirect("/")

###############################
# BLOG ENTRY
###################################
def blogentry(request, slug):  
    blog = get_object_or_404(BlogEntry, link = slug)
    image=get_img(blog.content)
    fb_head = blog.title
    form = BlogCommentForm(request.POST)
    bad_word = False
    absolut_url='http://'+request.get_host()+request.get_full_path()
    mobil='blog/one_blog.html'
    if request.method == 'POST':
        if form.is_valid():
            comm=form.save(commit=False)
            comm.blog=blog
            comm.user=request.user
            comm.save()
            if is_black_word(comm.comment):
                bad_word = True
                comm.delete()
            
    if is_mobile(request):
        mobil='blog/mobil_one_blog.html'
    return render_response(request, mobil, {
                                'blog': blog,
                                'url': absolut_url,
                                'headtitle': blog.title,
                                'description':blog.content,
                                'fb_head' :fb_head,
                                'image':image,
                                'is_admin': is_admin(request),
                                'comment':comment_show(request, slug),
                                'form': form,
                                'is_badword': bad_word,
                                'logged_in': check_user(request),
                          }, get_meta('/egy_blog'))
                          
   
def announcement(request, slug):
  announcement = get_object_or_404(Announcement, link = slug)
  
  return render_response(request, 'one_announcement.html', {
                              'announcement': announcement,
                        }, get_meta('/egy_blog'))
import re
def get_img(s):
    image_re = re.compile(r"""
                <img\s+                  # start of tag
                [^>]*?                   # non-src attributes
                src=                     # start of src attribute
                (?P<quote>["'])?         # optional opening quote
                (?P<image>[^"'>]+)       # image filename
                (?(quote)(?P=quote))     # closing quote
                [^>]*?                   # non-src attributes
                >                        # end of the tag
                 """, re.IGNORECASE|re.VERBOSE)
    image_files = []
    for match in image_re.finditer(s):
        image_files.append(match.group("image"))
    if image_files:
        return image_files[0]
    else:
        return None

def get_first_paragh(s):
    par = m=re.compile('<p>(.*?)</p>', re.DOTALL).findall(s)
    if par:
        for p in par:
          if (p!="\n\t" or p!="" ) and p.find("<img")==-1 and p.find("Sziasztok!")==-1:
              return p
    else:
        return None
                         
###################################
# COMMENT
###################################
def comment_show(request, slug):
    comments = None
    blog = None
    proj = None
    try:
        blog = BlogEntry.objects.get(link = slug)
        try:
            comments = BlogComment.objects.filter(blog = blog)
        except:
            comments = None            
    except:
        try:
            proj = Tender.objects.get(link = slug)
            try:
                comments = TenderComment.objects.filter(idea = proj)
            except:
                comments = None
        except:
            comments = None
    cms=[]
    if comments:
        for comm in comments:
            try:
                facebook = FacebookProfile.objects.get(user=comm.user)
                cms.append([True,comm])
            except:
                cms.append([False,comm])

    return cms

def comment_del(request, slug):
    comment = None
    blog = None
    try:
        comment = BlogComment.objects.get(slug = slug)
        blog = comment.blog
        if is_admin(request):
            comment.delete()
    except:
        try:
            comment = TenderComment.objects.get(slug = slug)
            if is_admin(request):
                comment.delete()
        except:
            pass
    return HttpResponseRedirect(request.session.get('HTTP_REFERER', None))
        
def is_black_word(string):
    bw = BlackWords.objects.all()
    for wd in bw:
        no=string.find(wd.word)
        if no>-1:
            return True

    return False
def list_all_comments(request):
    comments1 = None
    comments2 = None
    from datetime import datetime
    if is_admin(request):
        try:
            comments1 = BlogComment.objects.all()
            comments2 = TenderComment.objects.all()
        except:
            pass
        if request.method == 'POST':
            for i,com in enumerate(comments1):
                i=i+1
                if 'blog_%s'%i in request.POST and request.POST['blog_%s'%i]==com.slug:
                    com.delete()
            for i,com in enumerate(comments2):
                i=i+1
                if 'proj_%s'%i in request.POST and request.POST['proj_%s'%i]==com.slug:
                    com.delete()
        return render_response(request, 'comments.html', {
            'comments1': comments1,
            'comments2': comments2,
            }, get_meta('/'))
    else:
        return HttpResponseRedirect("/")

###################################
# BLOG LIST
###################################
def blog_tag(request, tag):
    query_tag = Tag.objects.get(name=tag)
    blogs = TaggedItem.objects.get_by_model(BlogEntry, query_tag)
    
    return render_response(request, 'blog/blogs.html', {
                                'blogs': blogs,
                                'tag': tag,
                          }, get_meta('/blog'))

def blog(request, page):
    BLOG_PAGING = settings.BLOG_PAGING
    page = int(page)
    
    blogs = BlogEntry.objects.filter(published = True).order_by('-created')[(page-1)*BLOG_PAGING:page*BLOG_PAGING]
 
    blog_paginator = Paginator(BlogEntry.objects.count(), BLOG_PAGING)
    blogss=[]
    for blog in blogs:
        img=get_img(blog.content)
        blog.content=get_first_paragh(blog.content)
        blogss.append([img,blog])
    mobil='blog/blogs.html'
    if is_mobile(request):
        mobil='blog/mobil_blogs.html'
  
    return render_response(request, mobil, {
                                'blogs': blogss,
                                'blog': blog_paginator,
                                'page': page,
                          }, get_meta('/blog'))


###################################
# IDEA LIST
###################################

def ideaorder(request, order_type):
    if order_type=='created':
        request.session['ordering'] = '-created'
    elif order_type=='popular':
        request.session['ordering'] =  '-supporters'
    elif order_type=='mobile':
        if not 'listonmobile' in request.session:
            request.session['listonmobile'] = True
            request.session['type_of_listing'] =  'is_mobile'
        elif request.session['listonmobile']:
            request.session['listonmobile'] = False
            request.session['type_of_listing'] =  ''
        else:
            request.session['listonmobile'] = True
            request.session['type_of_listing'] =  'is_mobile'
    else:
        if not 'listonweb' in request.session:
            request.session['listonweb'] = True
            request.session['type_of_listing'] =  'is_web'
        elif request.session['listonweb']:
            request.session['listonweb'] = False
            request.session['type_of_listing'] =  ''
        else:
            request.session['listonweb'] = True
            request.session['type_of_listing'] =  'is_web'

        
    return HttpResponseRedirect(request.session.get('HTTP_REFERER', None))    
    
def ideasbytag(request, tag):
    ideas = TaggedItem.objects.get_by_model(Tender, tag)
    idea_count = len(ideas)
    idea_paginator = Paginator(idea_count, 100)

    return render_response(request, 'idea/ideas.html', {
                                  'ideas': projdayvid,
                                  'otletek': idea_paginator,
                                  'number_of_ideas': idea_count,
                                  'page': 1,
                                  'order_by_created': '-created',
                                  'paginator_infix': 'szuro/mindegy/',

                            }, get_meta('/otletek'))
  
def ideasbymost(request, categ):
    request.session['categ']=categ
    category = NeccCategory.objects.get(link = categ)
    mostideas = MostTenders.objects.all().order_by('weight')
    nid_c=Tender.objects.all().filter(category = category, approved=True).order_by('-created').count()
    msst=[]
    mid_c=0
    for ms in mostideas:
        if ms.tender.category==category:
            msst.append(ms)
            mid_c+=1
    mostideas=msst[0:12]
    right=False
    if mid_c>6:
        right=True
    from datetime import datetime
    projdayvid=[]
    for pr in mostideas:
        left=pr.tender.expiry-datetime.now()
        lf=0
        orak=False
        if left.days==0:
            lf=left.seconds/3600
            orak=True
        elif left.days<0:
            lf=False
        else:
            lf=left.days
        try:
            sup = TenderSupporter.objects.all().filter(idea = pr.tender)
        except:
            sup=None
        now = 0
        percent = 0
        for i in sup:
            if i.anum:
                now+=i.money
        need=float(now)/float(pr.tender.full_money)
        percent=float(need)*100.0
        if need>=1.0:
            need=1
        projdayvid.append([pr.tender, lf, orak, now, need, round(percent,2)])    
    return render_response(request, 'base.html', {
        'projs':projdayvid,
        'right': right,
        'category': category,
        'is_admin': is_admin(request),
        'most': True,
        'index': range(1, 18)
    }, get_meta("/"))

def ideasbysucces(request, categ=None):
    category=None
    if categ:
        category = NeccCategory.objects.get(link = categ)
        ideas = Tender.objects.all().filter(category = category).order_by('-created')
    else:
        ideas = Tender.objects.all().order_by('-created')
    msst=[]
    collected_money_amounts=[]
    mid_c=0
    for idea in ideas:
        try:
            sup = TenderSupporter.objects.all().filter(idea = idea)
        except:
            sup=None
        now=0
        for i in sup:
            if i.anum:
                now+=i.money
        need=float(now)/float(idea.full_money)
        if need>=1.0 or idea.link == 'gulyas_peter_100000741662348_rezso_a_rezsifigyelo_alkalmazas_0':
            msst.append(idea)
            collected_money_amounts.append(now)
            mid_c+=1
    mostideas=msst[0:12]
    projdayvid=[]
    rightm=False
    from datetime import datetime
    project_idx = 0
    for pr in mostideas:
        left=pr.expiry-datetime.now()
        lf=0
        orak=False
        pr.approved=True
        if left.days==0:
            lf=left.seconds/3600
            orak=True
        elif left.days<0:
            lf=0
        else:
            lf=left.days
        try:
            sup = TenderSupporter.objects.all().filter(idea = idea)
        except:
            sup=None
        percent = 0
        percent=float(need)*100.0
        if need>=1.0:
            need=1
        projdayvid.append([pr, lf, orak, collected_money_amounts[project_idx], need, round(percent,2)]) 
        project_idx+=1

    if mid_c>3:
        rightm=True
    return render_response(request, 'idea/all_ideas.html', {
                                'now': now,
                                'foo': 'foobarfoobar',
                                'ideas': projdayvid,
                                'right': rightm,
                                'number_of_ideas': mid_c,
                                'succ':True,
                                'category':category,
                          }, get_meta('/otletek'))

def ideasbyfresh(request, categ=None):
    category=None
    if categ:
        category = NeccCategory.objects.get(link = categ)
        request.session['categ']=categ
        ideas = Tender.objects.all().filter(category = category, status__gte=2).order_by('status', '-created')
    else:
        if 'categ' in request.session:
            del request.session['categ']
        ideas = Tender.objects.all().filter(status__gte=2).order_by('status', '-created')
    right=False
    projdayvid=[]
    idea_count=0
    if ideas:           
        from datetime import datetime
        for pr in ideas:
            if pr.status==3 or pr.approved:
                left=pr.expiry-datetime.now()
                lf=0
                orak=False
                if left.days==0:
                    lf=left.seconds/3600
                    orak=True
                elif left.days<0:
                    lf=False
                else:
                    lf=left.days
                try:
                    sup = TenderSupporter.objects.all().filter(idea = pr)
                except:
                    sup=None
                now = 0
                percent = 0
                for i in sup:
                    if i.anum:
                        now+=i.money
                need=float(now)/float(pr.full_money)
                percent=float(need)*100.0
                if need>=1.0:
                    need=1
                idea_count+=1
                pr.approved=True
                projdayvid.append([pr, lf, orak, now, need, round(percent,2)])
                if len(projdayvid)>12:
                    break
    if idea_count>6:
        right=True
    return render_response(request, 'idea/all_ideas.html', {
                                'ideas': projdayvid[0:12],
                                'number_of_ideas': idea_count,
                                'right': right,
                                'category':category,
                          }, get_meta('/otletek'))    

def ideasbyend(request, categ=None):
    category=None
    if categ:
        category = NeccCategory.objects.get(link = categ)
        request.session['categ']=categ
        ideas = Tender.objects.all().filter(category = category, status = 3).order_by('-created')[0:12]
        idea_count = Tender.objects.all().filter(category = category, status = 3).count()
    else:
        if 'categ' in request.session:
            del request.session['categ']
        ideas = Tender.objects.all().filter(status = 3).order_by('-created')[0:12]
        idea_count = Tender.objects.all().filter(status = 3).count()
    right=False
    if idea_count>6:
        right=True
    projdayvid=[]  
    if ideas:           
        from datetime import datetime
        for pr in ideas:
            pr.approved=True
            left=pr.expiry-datetime.now()
            lf=0
            orak=False
            if left.days==0:
                lf=left.seconds/3600
                orak=True
            elif left.days<0:
                lf=False
            else:
                lf=left.days
            try:
                sup = TenderSupporter.objects.all().filter(idea = pr)
            except:
                sup=None
            now = 0
            percent = 0
            for i in sup:
                if i.anum:
                    now+=i.money
            need=float(now)/float(pr.full_money)
            percent=float(need)*100.0
            if need>=1.0:
                need=1
            projdayvid.append([pr, lf, orak, now, need, round(percent,2)])
        

    return render_response(request, 'idea/all_ideas.html', {
                                'ideas': projdayvid,
                                'number_of_ideas': idea_count,
                                'right': right,
                                'category':category,
                                'end': True,
                          }, get_meta('/otletek'))    

def ideasbyfin(request, categ=None):
    from datetime import datetime
    now=datetime.now()
    category=None
    if categ:
        category = NeccCategory.objects.get(link = categ)
        request.session['categ']=categ
        ideas = Tender.objects.all().filter(category=category, approved=True, expiry__gte=now).order_by('expiry')[0:12]
        idea_count = Tender.objects.all().filter(category=category, approved=True, expiry__gte=now).count()
    else:
        if 'categ' in request.session:
            del request.session['categ']
        ideas = Tender.objects.all().filter(approved=True, expiry__gte=now).order_by('expiry')[0:12]
        idea_count = Tender.objects.all().filter(approved=True, expiry__gte=now).count()
    right=False
    if idea_count>6:
        right=True
    projdayvid=[]  
    if ideas:
        for pr in ideas:
            left=pr.expiry-datetime.now()
            lf=0
            orak=False
            if left.days==0:
                lf=left.seconds/3600
                orak=True
            elif left.days<0:
                lf=False
            else:
                lf=left.days
            try:
                sup = TenderSupporter.objects.all().filter(idea = pr)
            except:
                sup=None
            now = 0
            percent = 0
            for i in sup:
                if i.anum:
                    now+=i.money
            need=float(now)/float(pr.full_money)
            percent=float(need)*100.0
            if need>=1.0:
                need=1
            projdayvid.append([pr, lf, orak, now, need, round(percent,2)])
        

    return render_response(request, 'idea/all_ideas.html', {
                                'ideas': projdayvid,
                                'number_of_ideas': idea_count,
                                'right': right,
                                'category': category,
                                'finish': True,
                          }, get_meta('/otletek'))     


def ideas(request,categ=None):
    category=None
    if categ:
        category = NeccCategory.objects.get(link = categ)
        ideas = Tender.objects.all().filter(category = category, approved=True).order_by('-created')[0:6]
        mostideas = MostTenders.objects.all().order_by('weight')
        nid_c=Tender.objects.all().filter(category = category, approved=True).order_by('-created').count()
        msst=[]
        mid_c=0
        for ms in mostideas:
            if ms.tender.category==category:
                msst.append(ms)
                mid_c+=1
        mostideas=msst[0:6]        
    else:
        ideas = Tender.objects.all().filter(approved=True).order_by('-created')[0:6]
        mostideas = MostTenders.objects.all().order_by('weight')[0:6]
        nid_c = Tender.objects.all().filter(approved=True).order_by('-created').count()
        mid_c = MostTenders.objects.all().order_by('weight').count()    
    projdayvid=[]
    rightm=False
    rightn=False   
    from datetime import datetime
    for pr in mostideas:
        left=pr.tender.expiry-datetime.now()
        lf=0
        orak=False
        if left.days==0:
            lf=left.seconds/3600
            orak=True
        elif left.days<0:
            lf=False
        else:
            lf=left.days
        try:
            sup = TenderSupporter.objects.all().filter(idea = pr.tender)
        except:
            sup=None
        now = 0
        percent = 0
        for i in sup:
            if i.anum:
                now+=i.money
        need=float(now)/float(pr.tender.full_money)
        percent=float(need)*100.0
        if need>=1.0:
            need=1
        projdayvid.append([pr.tender, lf, orak, now, need, round(percent,2)])    
    projdayvid2=[]
    if ideas:
        for pr in ideas:
            left=pr.expiry-datetime.now()
            lf=0
            orak=False
            if left.days==0:
                lf=left.seconds/3600
                orak=True
            elif left.days<0:
                lf=False
            else:
                lf=left.days
            try:
                sup = TenderSupporter.objects.all().filter(idea = pr)
            except:
                sup=None
            now = 0
            percent = 0
            for i in sup:
                if i.anum:
                    now+=i.money
            need=float(now)/float(pr.full_money)
            percent=float(need)*100.0
            if need>=1.0:
                need=1
            projdayvid2.append([pr, lf, orak, now, need, round(percent,2)])

    if mid_c>3:
        rightm=True
    if nid_c>3:
        rightn=True
    return render_response(request, 'idea/idea.html', {
                                'mostideas': projdayvid,
                                'freshideas': projdayvid2,
                                'rightm': rightm,
                                'szam': mid_c,
                                'rightn': rightn,
                                'category':category,
                          }, get_meta('/otletek'))

def mostideas(request,num,page,categ=None):
    if int(page)>1:
        p1=(int(page)-2)*int(num)
    else:
        p1=0
    p2=int(page)*int(num)*2
    category=None
    if categ:
        category = NeccCategory.objects.get(link = categ)
        ideas = MostTenders.objects.all().order_by('weight')
        msst=[]
        ideacount=0
        for ms in ideas:
            if ms.tender.category==category:
                msst.append(ms)
                ideacount+=1
        ideas = msst[p1:p2]
    else:
        ideas = MostTenders.objects.all().order_by('weight')[p1:p2]
        ideacount = MostTenders.objects.all().order_by('weight').count()
    projdayvid=[]
    before=False
    after=False
    if int(page)>1:
        before=True
    if (int(page)*int(num))<ideacount:
        after=True
    if ideas:           
        from datetime import datetime
        for pr in ideas:
            if category and pr.tender.category==category or not category:
                left=pr.tender.expiry-datetime.now()
                lf=0
                orak=False
                if left.days==0:
                    lf=left.seconds/3600
                    orak=True
                elif left.days<0:
                    lf=False
                else:
                    lf=left.days
                try:
                    sup = TenderSupporter.objects.all().filter(idea = pr.tender)
                except:
                    sup=None
                now = 0
                percent = 0
                for i in sup:
                    if i.anum:
                        now+=i.money
                need=float(now)/float(pr.tender.full_money)
                percent=float(need)*100.0
                if need>=1.0:
                    need=1
                projdayvid.append([pr.tender, lf, orak, now, need, round(percent,2)])
    
    return render_response(request, 'idea/idea_scroll.html', {
                                'ideas': projdayvid,
                                'pbef': int(page)-1,
                                'paft': int(page)+1,
                                'before': before,
                                'after': after,
                                'most' : True,
                                'num':num,
                                'category': category,
                          }, get_meta('/otletek'))

def succesideas(request,num,page,categ=None):
    if int(page)>1:
        p1=(int(page)-2)*int(num)
    else:
        p1=0
    p2=int(page)*int(num)*2
    category=None
    if categ:
        category = NeccCategory.objects.get(link = categ)
        ideas = Tender.objects.all().filter(category = category).order_by('-created')
    else:
        ideas = Tender.objects.all().order_by('-created')
    msst=[]
    mid_c=0
    ideacount=0
    for idea in ideas:
        ideacount += 1
        try:
            sup = TenderSupporter.objects.all().filter(idea = idea)
        except:
            sup=None
        now=0
        for i in sup:
            if i.anum:
                now+=i.money
        need=float(now)/float(idea.full_money)
        if need>=1.0:
            msst.append(idea)
            mid_c+=1
    mostideas=msst[p1:p2]        
    projdayvid=[]
    rightm=False
    from datetime import datetime
    for pr in mostideas:
        left=pr.expiry-datetime.now()
        lf=0
        orak=False
        pr.approved=True
        if left.days==0:
            lf=left.seconds/3600
            orak=True
        elif left.days<0:
            lf=0
        else:
            lf=left.days
        percent = 0
        percent=float(need)*100.0
        if need>=1.0:
            need=1
        projdayvid.append([pr, lf, orak, now, need, round(percent,2)])    

    if int(page)>1:
        before=True
    if (int(page)*int(num))<ideacount:
        after=True
    return render_response(request,'idea/idea_scroll.html', {
                                'ideas': projdayvid,
                                'pbef': int(page)-1,
                                'paft': int(page)+1,
                                'before': before,
                                'after': after,
                                'freash': True,
                                'succ': True,
                                'num':num,
                                'category': category,
                          }, get_meta('/otletek'))

def freshideas(request,num,page,categ=None):
    if int(page)>1:
        p1=(int(page)-2)*int(num)
    else:
        p1=0
    p2=int(page)*int(num)*2
    category=None
    if categ:
        category = NeccCategory.objects.get(link = categ)
        ideas = Tender.objects.all().filter(category=category, status__gte=2).order_by('status', '-created')
    else:
        ideas = Tender.objects.all().filter(status__gte=2).order_by('status', '-created') 
    projdayvid=[]
    ideacount=0
    before=False
    after=False
    if ideas:           
        from datetime import datetime
        for pr in ideas:
            if pr.approved or pr.status==3:
                pr.approved=True
                left=pr.expiry-datetime.now()
                lf=0
                orak=False
                if left.days==0:
                    lf=left.seconds/3600
                    orak=True
                elif left.days<0:
                    lf=False
                else:
                    lf=left.days
                try:
                    sup = TenderSupporter.objects.all().filter(idea = pr)
                except:
                    sup=None
                now = 0
                percent = 0
                for i in sup:
                    if i.anum:
                        now+=i.money
                need=float(now)/float(pr.full_money)
                percent=float(need)*100.0
                if need>=1.0:
                    need=1
                projdayvid.append([pr, lf, orak, now, need, round(percent,2)])
                ideacount+=1
    if int(page)>1:
        before=True
    if (int(page)*int(num))<ideacount:
        after=True
    return render_response(request, 'idea/idea_scroll.html', {
                                'ideas': projdayvid[p1:p2],
                                'pbef': int(page)-1,
                                'paft': int(page)+1,
                                'before': before,
                                'after': after,
                                'fresh': True,
                                'num':num,
                                'category': category,
                          }, get_meta('/otletek'))

def endideas(request,num,page,categ=None):
    if int(page)>1:
        p1=(int(page)-2)*int(num)
    else:
        p1=0
    p2=int(page)*int(num)*2
    category=None
    if categ:
        category = NeccCategory.objects.get(link = categ)
        ideas = Tender.objects.all().filter(category=category, status=3).order_by('-created')[p1:p2]
        ideacount = Tender.objects.all().filter(category=category, status=3).order_by('-created').count()
    else:
        ideas = Tender.objects.all().filter(status=3).order_by('-created')[p1:p2]        
        ideacount = Tender.objects.all().filter(status=3).order_by('-created').count()    
    projdayvid=[]
    before=False
    after=False
    if int(page)>1:
        before=True
    if (int(page)*int(num))<ideacount:
        after=True
    if ideas:           
        from datetime import datetime
        for pr in ideas:
            pr.approved=True
            left=pr.expiry-datetime.now()
            lf=0
            orak=False
            if left.days==0:
                lf=left.seconds/3600
                orak=True
            elif left.days<0:
                lf=False
            else:
                lf=left.days
            try:
                sup = TenderSupporter.objects.all().filter(idea = pr)
            except:
                sup=None
            now = 0
            percent = 0
            for i in sup:
                if i.anum:
                    now+=i.money
            need=float(now)/float(pr.full_money)
            percent=float(need)*100.0
            if need>=1.0:
                need=1
            projdayvid.append([pr, lf, orak, now, need, round(percent,2)])
    
    return render_response(request, 'idea/idea_scroll.html', {
                                'ideas': projdayvid,
                                'pbef': int(page)-1,
                                'paft': int(page)+1,
                                'before': before,
                                'after': after,
                                'end': True,
                                'num':num,
                                'category': category,
                          }, get_meta('/otletek'))

def finideas(request,num,page,categ=None):
    if int(page)>1:
        p1=(int(page)-2)*int(num)
    else:
        p1=0
    p2=int(page)*int(num)*2
    from datetime import datetime
    now=datetime.now()
    category=None
    if categ:
        category = NeccCategory.objects.get(link = categ)
        ideas = Tender.objects.all().filter(category=category, approved=True, expiry__gte=now).order_by('expiry')[p1:p2]
        ideacount = Tender.objects.all().filter(category=category, approved=True, expiry__gte=now).order_by('expiry').count()
    else:
        ideas = Tender.objects.all().filter(approved=True, expiry__gte=now).order_by('expiry')[p1:p2]
        ideacount = Tender.objects.all().filter(approved=True, expiry__gte=now).order_by('expiry').count()    
    projdayvid=[]
    before=False
    after=False
    if int(page)>1:
        before=True
    if (int(page)*int(num))<ideacount:
        after=True
    if ideas:           
        #from datetime import datetime
        for pr in ideas:
            left=pr.expiry-datetime.now()
            lf=0
            orak=False
            if left.days==0:
                lf=left.seconds/3600
                orak=True
            elif left.days<0:
                lf=False
            else:
                lf=left.days
            try:
                sup = TenderSupporter.objects.all().filter(idea = pr)
            except:
                sup=None
            now = 0
            percent = 0
            for i in sup:
                if i.anum:
                    now+=i.money
            need=float(now)/float(pr.full_money)
            percent=float(need)*100.0
            if need>=1.0:
                need=1
            projdayvid.append([pr, lf, orak, now, need, round(percent,2)])
    
    return render_response(request, 'idea/idea_scroll.html', {
                                'ideas': projdayvid,
                                'pbef': int(page)-1,
                                'paft': int(page)+1,
                                'before': before,
                                'after': after,
                                'fin': True,
                                'num':num,
                                'category': category,
                          }, get_meta('/otletek'))

def topten(request):
    ideas = Tender.objects.all()[:10]

    return render_response(request, 'idea/topten.html', {
                                'ideas': ideas,
                          }, get_meta('/otletek'))

def all_ideas(request):
    ideas = Tender.objects.all()

    return render_response(request, 'idea/all_ideas.html', {
                                'ideas': ideas,
                          }, get_meta('/otletek'))
def gallery(request):
    return render_response(request, 'dijatado_2011.html', {                                
                          }, get_meta('/kepek'))   

###################################
# IDEA ENTRY
###################################
def unfinished_projects(request):
    ideas = Tender.objects.all().filter(approved=False, status=2)
    for pr in ideas:
        send_mail(u"admin@creativeselector.hu", pr.owner.email,
                          u"projekt indítása" , "email/unfinished_projekt",
                          "email/unfinished_projekt", {'username' : u"%s %s" % (pr.owner.last_name, pr.owner.first_name),
                                                       'proj' : pr,
                                                       'url': u"http://%s/projekt_szerk/%s" % (request.get_host(),pr.link ),})
    return render_response(request, 'facebook/show_string.html', {
        'string': "emails sended"
        }, get_meta(''))

def send_project(request):
    page = Staticpage.objects.get(link = u"uj_projekt_inditasa")
    return render_response(request, 'projekt/send_projekt.html', {
                    'new_projekt_text':page,
        }, get_meta('/egy_otlet'))   

def send_project2(request):
    try:
        tos = Staticpage.objects.get(link = u"felhasznalasi_feltetelek")
    except:
        tos = None
    return render_response(request, 'projekt/send_projekt2.html', {
                   'tos':tos,
        }, get_meta('/egy_otlet'))   
def send_project3(request):
    class RequiredFormSet(BaseFormSet):
        def __init__(self, *args, **kwargs):
            super(RequiredFormSet, self).__init__(*args, **kwargs)
            for form in self.forms:
                form.empty_permitted = False
    form = TenderForm()
    categories = NeccCategory.objects.all()
    PresentFormSet = formset_factory(TenderpresentForm, max_num=10, formset=RequiredFormSet)
    data = {'form-TOTAL_FORMS': u'4','form-INITIAL_FORMS': u'4','form-MAX_NUM_FORMS': u'10', }
    tenderpresents = PresentFormSet(data)
    
    if request.method == "POST":
        form = TenderForm(request.POST)
        tenderpresents = PresentFormSet(request.POST)
        if form.is_valid():
            proj=form.save(commit=False)
            lnk = slugify(u"%s - %s - %s" % (proj.owner, proj.short_title, proj.serial))
            prj=None
            try:
                prj=Tender.objects.get(link=lnk)
            except:
                pass
            if not prj:
                proj.save()
                for form in tenderpresents.forms:
                    if form.is_valid():
                        pres = form.save(commit=False)
                        pres.tender=proj
                        pres.save()
                    else:
                        proj.delete()
                        return show_message(request, "Ooopsz", "valami baki lépett közbe, kérlek próbáld <a href=\"javascript:window.history.back();\">újra</a><br/>nézd meg, hogy az ajándékoknál mindent jól töltöttél-e ki!")
                send_mail(u"admin@creativeselector.hu", proj.owner.email,
                          u"%s - projekted tervét megkaptuk" % proj.short_title , "email/new_projekt_owner_wait",
                          "email/new_projekt_owner_wait", {'username' : u"%s %s" % (proj.owner.last_name, proj.owner.first_name)})
                send_mail(u"admin@creativeselector.hu", u"projekt@creativeselector.hu",
                          u"Új Projekt: %s elbírálásra várva" % proj.short_title , "email/new_projekt_admin_wait", "email/new_projekt_admin_wait",
                          {'projekt': proj, 'url' : u"http://%s/activate_proj/%s" % (request.get_host(),proj.link )})
                npr=NewTenderAdmin(proj=proj)
                npr.save()
                return show_message(request, "Sikeres beküldés", "Köszönjük, projekt-terved megkaptuk. Hamarosan jelentkezünk!")
            else:
                return show_message(request, "Hiba a beküldésben", "Kérlek értesísd az adminisztrátort")
        
        return show_message(request, "Hiba a beküldésben", "Kérlek értesísd az adminisztrátort")

    else:
        return render_response(request, 'projekt/send_projekt3.html', {
                   'form':form,
                   'categories': categories,
                   'forms_present': tenderpresents,
                   'logged_in': check_user(request),
        }, get_meta('/egy_otlet'))

def validate_project(request, slug):
    if is_super(request):
        proj=get_object_or_404(Tender, link = slug)
        form=ValidateForm()
        presents=None
        try:
            presents=TenderPresent.objects.all().filter(tender = proj).order_by("min_prize")
        except:
            pass
        return render_response(request, 'projekt/proj_activation.html', {
                    'form' : form,
                    'projekt': proj,
                    'presents': presents,
                    'is_admin': is_admin(request),
            }, get_meta('/egy_otlet'))
    else:
        return HttpResponseRedirect("/")

def validate_project_ok(request, pk):
    proj=get_object_or_404(Tender, pk = pk)
    proj.status=2
    proj.introduce=" "
    proj.why_sup=" "
    proj.faceurl=""
    proj.save()
    send_mail(u"admin@creativeselector.hu", proj.owner.email, u"%s - projekt befogadása" % proj.short_title ,
              "email/proj_ok", "email/proj_ok",
              { 'username' : u"%s %s" % (proj.owner.last_name, proj.owner.first_name),
                'proj' : proj,
                'url': u"http://%s/projekt_folyt/%s" % (request.get_host(),proj.link ),
                'url2': u"http://%s/profilom" % (request.get_host())})    
    return HttpResponseRedirect("/")

def validate_project_no(request, pk):
    if request.method == "POST":
        form=ValidateForm(request.POST)
        proj = get_object_or_404(Tender, pk = pk)
        if form.is_valid():
            mailtext = form.cleaned_data['mail_text']
            send_mail(u"admin@creativeselector.hu", proj.owner.email, u"A %s projektedet elutasítottuk" % proj.short_title,
                      "email/proj_no", "email/proj_no",
                      {'username' : u"%s" % proj.owner.username, 'text': mailtext})        
    return HttpResponseRedirect("/")

def project_continue(request, slug):
    proj = get_object_or_404(Tender, link = slug)
    page = Staticpage.objects.get(link = u"projekt_inditasa2")
    return render_response(request, 'projekt/proj_szerk.html', {
                    'new_projekt_text':page,
                    'proj': proj,
        }, get_meta('/egy_otlet'))   

def continue_project(request, slug):
    proj = get_object_or_404(Tender, link = slug)
    categories = NeccCategory.objects.all()
    owner = False
    accept = False
    form = TenderForm()
    if request.user == proj.owner or is_admin(request):
        owner = True
        if proj.status == 2 and not proj.approved:
            accept = True
            proj = get_object_or_404(Tender, link = slug)
            form = TenderForm(instance = proj)
        else:
            proj = None
            form = None
    else:
        proj = None
        form = None
    if request.method == "POST":
        form = TenderForm(request.POST, instance = proj)
        if form.is_valid():
            proj2 = form.save(commit=False)
            proj2 = proj
            proj2.save()
            return HttpResponseRedirect(reverse("creativeselector.views.continue_project2", args = [proj.link]))
        return show_message(request, "Hibás az oldal", "Hibás:  %s<br/> kérlek jelezd az adminisztrátornak" % (form.errors))
    else:
        return render_response(request, 'projekt/proj_szerk1.html', {
                                  'proj': proj,
                                  'form': form,                                  
                                  'categories': categories,
                                  'logged_in': check_user(request),
                            }, get_meta('/egy_otlet'))  

def continue_project2(request, slug):
    proj = get_object_or_404(Tender, link = slug)
    owner = False
    accept = False
    form = TenderForm()
    if request.user == proj.owner or is_admin(request):
        owner = True
        if proj.status == 2 and not proj.approved:
            accept = True
            proj = get_object_or_404(Tender, link = slug)
            form = TenderForm(instance = proj)
        else:
            proj = None
            form = None
    else:
        proj = None
        form = None
    if request.method == "POST":
        form = TenderForm(request.POST, instance = proj)
        if form.is_valid():
            proj2 = form.save(commit=False)
            proj2 = proj
            proj2.save()
            return HttpResponseRedirect(reverse("creativeselector.views.continue_project2_2", args = [proj.link]))
        return show_message(request, "Hibás az oldal", "Hibás:  %s<br/> kérlek jelezd az adminisztrátornak" % (form.errors))
    else:
        return render_response(request, 'projekt/proj_szerk2.html', {
                                  'proj': proj,
                                  'form': form,
                                  'logged_in': check_user(request),
                            }, get_meta('/egy_otlet'))  
def continue_project2_2(request, slug):
    proj = get_object_or_404(Tender, link = slug)
    owner = False
    accept = False
    form = TenderForm()
    if request.user == proj.owner or is_admin(request):
        owner = True
        if proj.status == 2 and not proj.approved:
            accept = True
            proj = get_object_or_404(Tender, link = slug)
            form = TenderForm(instance = proj)
        else:
            proj = None
            form = None
    else:
        proj = None
        form = None
    if request.method == "POST":
        form = TenderForm(request.POST, instance = proj)
        if form.is_valid():
            proj2 = form.save(commit=False)
            proj2 = proj
            proj2.save()
            return HttpResponseRedirect(reverse("creativeselector.views.continue_project3", args = [proj.link]))
        return show_message(request, "Hibás az oldal", "Hibás:  %s<br/> kérlek jelezd az adminisztrátornak" % (form.errors))
    else:
        return render_response(request, 'projekt/proj_szerk2_v.html', {
                                  'proj': proj,
                                  'form': form,
                                  'logged_in': check_user(request),
                            }, get_meta('/egy_otlet'))  

def continue_project3(request, slug):
    proj = get_object_or_404(Tender, link = slug)
    owner = False
    accept = False
    form = TenderForm()
    bud=None
    try:
        bud=TenderBudget.objects.get(tender = proj)
    except:
        bud=None
    if request.user == proj.owner or is_admin(request):
        owner = True
        if proj.status == 2 and not proj.approved:
            accept = True
            proj = get_object_or_404(Tender, link = slug)
            form = TenderForm(instance = proj)
        else:
            proj = None
            form = None
    else:
        proj = None
        form = None
    if request.method == "POST":
        form = TenderForm(request.POST, instance = proj)
        if form.is_valid():
            proj2 = form.save(commit=False)
            proj2 = proj
            proj2.save()
            invoice_no = request.POST['invoice_no']
            if invoice_no:
                if bud:
                    bud.invoice_no=invoice_no
                    bud.money=proj2.full_money
                    bud.save()
                else:
                    budget = TenderBudget(tender=proj2, owner=proj2.owner, invoice_no=invoice_no, money=proj2.full_money)
                    budget.save()
            return HttpResponseRedirect(reverse("creativeselector.views.continue_project4", args = [proj.link]))
        return show_message(request, "Hibás az oldal", "Hibás:  %s<br/> kérlek jelezd az adminisztrátornak" % (form.errors))
    else:
        return render_response(request, 'projekt/proj_szerk3.html', {
                                  'proj': proj,
                                  'budget' : bud,
                                  'form': form,
                                  'logged_in': check_user(request),
                            }, get_meta('/egy_otlet'))  

def continue_project4(request, slug):
    proj = get_object_or_404(Tender, link = slug)
    presents = TenderPresent.objects.all().filter(tender = proj).order_by("min_prize")
    #present_no = TenderPresent.objects.all().filter(tender = proj).order_by("min_prize").count()
    owner = False
    accept = False
    tenderpresents = None
    if request.user == proj.owner or is_admin(request):
        owner = True
        if proj.status == 2 and not proj.approved:
            accept = True            
            if presents:
                PresentFormSet = modelformset_factory(TenderPresent, form=TenderpresentForm, max_num=10, extra=0)
                tenderpresents = PresentFormSet(queryset = presents)
            else:
                PresentFormSet = modelformset_factory(TenderPresent, form=TenderpresentForm, max_num=10, extra=3)
                tenderpresents = PresentFormSet(queryset = TenderPresent.objects.none())
        else:
            proj = None
            presents = None
    else:
        proj = None
        presents = None
    if request.method == "POST":
        tenderpresents = PresentFormSet(request.POST, queryset = presents)        
        if tenderpresents.is_valid():
            forms=tenderpresents.save(commit=False)
            for form in forms:
                if not form.id:
                    pr = TenderPresent(tender=proj, min_prize=form.min_prize, detail=form.detail)
                    pr.save()
                else:
                    form.save()
                
            return HttpResponseRedirect(reverse("creativeselector.views.continue_project5", args = [proj.link]))
        return show_message(request, "Hibás az oldal", "Hibás:  %s<br/> kérlek jelezd az adminisztrátornak" % (tenderpresents.errors))
    else:
        return render_response(request, 'projekt/proj_szerk4.html', {
                                  'proj': proj,
                                  'formset': tenderpresents,
                                  'logged_in': check_user(request),
                            }, get_meta('/egy_otlet'))  

def continue_project5(request, slug):
    proj = get_object_or_404(Tender, link = slug)
    picnum = None
    publicable=False
    vid_url=None
    popup=False
    if proj.video_url:
        vid_url=proj.video_url
        if proj.video_url.find('picnum:')>=0:
            h = proj.video_url.split('picnum:')
            picnum = h[1]
            proj.video_url=None
    else:
        try:
            pics=TenderImage.objects.all().filter(image=proj)
            if pics:
                vid_url=True
        except:
            pass
    from datetime import datetime
    if proj.expiry:
        lefttime = proj.expiry-datetime.now()
    else:
        lefttime = datetime.now()-datetime.now()
    presents = TenderPresent.objects.all().filter(tender = proj).order_by("min_prize")
    if 'popup' in request.session and request.session['popup']:
        popup=True
        request.session['popup']=False
    if proj.introduce and proj.expiry and vid_url and presents and proj.main_pic:
        publicable=True
    return render_response(request, 'projekt/proj_szerk5.html', {
                                  'proj': proj,
                                  'presents': presents,
                                  'daysleft' : lefttime.days,
                                  'picnum' : picnum,
                                  'publicable': publicable,
                                  'logged_in': check_user(request),
                                  'is_admin':is_admin(request),
                                  'popup': popup,
                            }, get_meta('/egy_otlet'))  
def saveproject(request, slug, publ):
    proj = get_object_or_404(Tender, link = slug)
    if request.user == proj.owner:
        pub=False
        if publ=="True":
            pub=True
        if pub:
            #proj.approved=True
            proj.visitors=1
            proj.save()
            send_mail(u"admin@creativeselector.hu", u"projekt@creativeselector.hu",
                          u"Élesítésre váró Projekt: %s" % proj.short_title , "email/new_projekt_admin_wait", "email/new_projekt_admin_wait",
                          {'projekt': proj, 'active': True, 'url' : u"http://%s%s" % (request.get_host(),reverse("creativeselector.views.continue_project5", args = [proj.link]) )})
            return show_message(request, "Projekt aktiválásra vár", "<p>Pár pillanat múlva átnézzük a kész projekted.<br>Ha mindent rendben találunk aktiváljuk, vagy további kiegészítéseket kérünk.<br>Mindkét esetben hamarosan megy az email.</p><p>Köszönjük türelmedet!</p>")
        else:
            proj.save()
            request.session['popup'] = True
            return HttpResponseRedirect(reverse("creativeselector.views.continue_project5", args = [proj.link]))
    else:
        return HttpResponseRedirect("/")

def projectpublic(request, slug):
    if is_admin(request):
        from datetime import datetime
        proj = get_object_or_404(Tender, link = slug)
        proj.approved=True
        proj.created=datetime.now()
        proj.save()
        send_mail(u"admin@creativeselector.hu", proj.owner.email,
                          u"%s projekted aktiváltuk" % proj.short_title , "email/proj_ok", "email/proj_ok",
                          {'projekt': proj, 'active': True, 'url' : u"http://%s%s" % (request.get_host(),reverse("creativeselector.views.get_project", args = [proj.link]) )})
    return HttpResponseRedirect(request.session.get('HTTP_REFERER', None))  

def projecttomost(request, slug):
    if is_admin(request):
        proj = get_object_or_404(Tender, link = slug)
        mdel=None
        try:
            mdel=MostTenders.objects.get(tender=proj)
        except:
            pass
        if mdel:
            mdel.delete()
        else:
            mproj = MostTenders(tender = proj)
            mproj.save()
    return HttpResponseRedirect(request.session.get('HTTP_REFERER', None))  

def get_project(request, slug):
    if slug == 'lili_eszter_tillmann_1185206403_muvelodesz_szint_mu_szi_0':
        slug='lili_eszter_tillmann_1185206403_muvelodesi_szint_mu_szi_0'
    proj = get_object_or_404(Tender, link = slug)
    if proj.approved or proj.status==3:
        mproj=False
        fb_pic='http://%s/upload/download/%s/creativeselector.tender/main_pic/'% (request.get_host(),proj.pk)
        from datetime import datetime
        lefttime = proj.expiry-datetime.now()
        left = 0
        hours = False
        if lefttime.days>0:
            left=lefttime.days
        if lefttime.days==0:
            hours = True
        presents = TenderPresent.objects.all().filter(tender = proj).order_by("min_prize")
        sup = TenderSupporter.objects.all().filter(idea = proj)
        tdu = TenderUpdate.objects.all().filter(idea = proj).order_by('-created')
        if tdu:
            tdu=tdu[0]
        form = TenderCommentForm()
        try:
            mpr=MostTenders.objects.get(tender=proj)
            mproj=True
        except:
            pass
        now = 0
        percent = 0
        sprs=[]
        spdrs=[]
        no_sup = 0
        none_pres=0
        for i in sup:
            if i.anum:
                now+=i.money
                no_sup+=1                
                if i.user not in spdrs:
                    sprs.append(i)
                    spdrs.append(i.user)
        need=float(now)/float(proj.full_money)
        percent=float(need)*100.0
        url='http://'+request.get_host()+request.get_full_path()
        if need>=1.0:
            need=1
        pres=[]
        num_pres=[]
        pr=presents[0]
        non_pr=0
        if pr.min_prize>500:
            nm_pres = TenderSupporter.objects.all().filter(idea = proj, money__lt=pr.min_prize)
            for i in nm_pres:
                if i.anum:
                    non_pr+=1
        nt=no_sup-non_pr
        num_pres.append([None,non_pr,nt])
        pres.append([pr,nt])
        counter = 0
        for pr in presents:
            no = 0
            #try:
            nm_pres = TenderSupporter.objects.all().filter(idea = proj, money__gte=pr.min_prize)
            for i in nm_pres:
                if i.anum:
                    no+=1
            #except:
                #no = 0
            if counter>0:
                npt=nt-no
                num_pres.append([pres[counter-1][0],npt,no])
                nt=nt-npt
                pres.append([pr,no])                
            counter+=1
        npt=pres[counter-1][1]
        num_pres.append([pres[counter-1][0],npt,0])
        num_pres=num_pres[1:]
        bad_word=False
        #non_pres=no_sup-with_pres
        if request.method == 'POST':
            form = TenderCommentForm(request.POST)
            if form.is_valid():
                comm=form.save(commit=False)
                comm.idea=proj
                comm.user=request.user
                comm.save()
                if is_black_word(comm.comment):
                    bad_word = True
                    comm.delete()
                else:
                    if comm.user!=proj.owner:
                        send_mail(u"admin@creativeselector.hu", proj.owner.email,
                                  u"Komment érkezett a %s projekthez" % proj.short_title , "email/com", "email/com",
                                  {'com': comm,  'url' : u"http://%s%s#comments" % (request.get_host(),reverse("creativeselector.views.get_project", args = [proj.link]) )})
        return render_response(request, 'projekt/proj_show.html', {
                                      'proj': proj,
                                      'now': now,
                                      'need': need,
                                      'pageurl' : url,
                                      'percent': round(percent,2),
                                      'supporters' : no_sup,
                                      'form':form,
                                      'tdu' : tdu,
                                      'fb_head' :proj.short_title,
                                      'fb_desc': proj.introduce,
                                      'image' : fb_pic,
                                      'sp_users' : sprs,
                                      'is_badword': bad_word,
                                      'is_admin':is_admin(request),
                                      'mproj' : mproj,
                                      'comment':comment_show(request, slug),
                                      'daysleft' : left,
                                      'hours' : hours,
                                      'presents': num_pres,
                                      'logged_in': check_user(request),
                                }, get_meta('/egy_otlet'))
    else:
        return HttpResponseRedirect("/")

def owner_project(request, slug):
    proj = get_object_or_404(Tender, link = slug)
    if request.user == proj.owner or is_admin(request):
        presents = TenderPresent.objects.all().filter(tender = proj).order_by("min_prize")
        supps = TenderSupporter.objects.all().filter(idea = proj)        
        form = TenderupdateForm()
        real_supps=[]
        sups=[]
        for sup in supps:
            if sup.pres and sup.anum:
                counter=1
                for pres in presents:
                    if sup.pres.id==pres.id:
                        real_supps.append([str(counter),sup])
                    counter+=1
            if sup.anum:
                sups.append(sup)
        if request.method == "POST":
            form = TenderupdateForm(request.POST)
            for i,sp in enumerate(supps):
                i=i+1
                if 'sp_%s'%i in request.POST and request.POST['sp_%s'%i]==sp.trid:
                    sp.get_pres=True
                    sp.save()
            if form.is_valid():
                tdu=form.save(commit=False)
                tdu.idea=proj
                tdu.link=u"%s" % proj.link
                tdu.save()
                sup_usr=[]
                for sp in supps:
                    if sp.user not in sup_usr:
                        if not sp.subscribed:
                            send_mail(u"admin@creativeselector.hu", sp.user.email, u"A %s projekthez frissítés érkezett" % proj.short_title,
                                      "email/proj_update", "email/proj_update",
                                      {'username' : u"%s %s" % (sp.user.last_name, sp.user.first_name),
                                       'proj' : proj,
                                       'tdu' : tdu,
                                       'link': u"http://%s/projekt/update/%s" % (request.get_host(),proj.link )})
                        sup_usr.append(sp.user)
        tdus = TenderUpdate.objects.all().filter(idea = proj).order_by('-created')
        if tdus:
            tdus=tdus[0]
        return render_response(request, 'projekt/owner_proj_list.html', {
                                      'proj': proj, 
                                      'presents': presents,
                                      'supp_pres' : real_supps,
                                      'supps': sups,
                                      'form' : form,
                                      'tdus' : tdus,
                                }, get_meta('/egy_otlet'))
    else:
        return HttpResponseRedirect("/")

def get_project_update(request, slug):
    proj = get_object_or_404(Tender, link = slug)
    prup = TenderUpdate.objects.all().filter(idea = proj).order_by('-created')
    fb_pic='http://%s/upload/download/%s/creativeselector.tender/main_pic/'% (request.get_host(),proj.pk)
    detail=''
    if prup:
        detail=prup[0].content
    url = 'http://'+request.get_host()+request.get_full_path()
    return render_response(request, 'projekt/update.html', {
                                  'prup': prup,
                                  'uri':url,
                                  'fb_head' :u"%s - Projekt Frissítés" % proj.short_title,
                                  'fb_desc': detail,
                                  'image' : fb_pic,
                                  'proj': proj,
                            }, get_meta('/egy_otlet'))

def projekt_supporters(request, slug):
    proj = get_object_or_404(Tender, link = slug)
    sup = TenderSupporter.objects.all().filter(idea = proj)
    spres=[]
    for sp in sup:
        if sp.anum:
            spres.append(sp)
    return render_response(request, 'projekt/proj_sup_list.html', {
                                  'proj': proj,
                                  'supps' : spres,                                  
                            }, get_meta('/egy_otlet'))

def unsubscribe_from_proj(request, slug):
    proj = get_object_or_404(Tender, link = slug)
    if check_user(request):
        sup = TenderSupporter.objects.all().filter(user = request.user,  idea=proj)
        for sp in sup:
            sp.subscribed=False
            sp.save()
        return show_message(request, "Sikeres leiratkozás", "Sikeresen leíratkoztál a %s projekt frissítéseiről" % (proj.short_title))
    return show_message(request, "Leiratkozás", "Előbb jelentkezz be!")

def remove_tenderimage(request, pk):
    tenderimage = get_object_or_404(TenderImage, pk = long(pk))
    tenderimage.delete()
    return HttpResponseRedirect(request.session.get('HTTP_REFERER', None))

def end_projekts(request):
    from datetime import datetime
    now=datetime.now()
    ends=Tender.objects.all().filter(expiry__lte=now).order_by('expiry')
    #text="Lejárt projektek:<br/><ol>"
    for pr in ends:
        if pr.status==2 and pr.approved:
            pr.status=3
	    pr.approved=False
	    pr.save()
	    mdel=None
	    try:
                mdel=MostTenders.objects.get(tender=pr)
	    except:
		pass
	    if mdel:
		mdel.delete()
	    #text+= u"<li> %s </li>" % (pr.link.decode("utf8"))
    #text+="</ol>"
    return render_response(request, 'facebook/show_string.html', {
        'string': "Lejárt projetek frissítve"
        }, get_meta(''))    
#####################################
# Support Projekt
#####################################
def present_form(request, slug):
    proj = get_object_or_404(Tender, link = slug)
    from datetime import datetime
    lefttime = proj.expiry-datetime.now()
    if lefttime.days>=0:
        presents = TenderPresent.objects.all().filter(tender = proj).order_by("min_prize")
        suppres = []
        avg=0
        per=TenderSupporter.objects.all().filter(idea = proj).count()
        if not per:
            per=1
        sup=TenderSupporter.objects.all().filter(idea = proj)
        with_pres=0
        for pres in presents:
            no=0
            try:
                nop=TenderSupporter.objects.all().filter(idea = proj, pres = pres)
                for i in nop:
                    if i.anum:
                        no+=1
            except:
                no=0
            suppres.append([pres, no])
            with_pres+=no
        all_sup=0
        for i in sup:
            if i.anum:
                avg+=i.money
                all_sup+=1
        avg=avg/per
        non_pres=all_sup-with_pres
        template = 'present/proj_pres.html'
        form = TendersupportForm()
        if request.method == "POST":
            form = TendersupportForm(request.POST)
            if form.is_valid():
                sup=form.save(commit=False)
                pres = request.POST['pres_type']
                vis = request.POST['visb']
                if pres:
                    if pres!='0':
                        present=get_object_or_404(TenderPresent, pk = pres)
                        sup.pres=present
                if vis:
                    sup.visibility=vis
                try:
                    bud=Tenderbudget.objects.get(idea = proj)
                    bud.money_now+=pres.money
                    bud.save()
                except:
                    pass
                sup.idea=proj
                sup.save()
                return HttpResponseRedirect(reverse("creativeselector.views.send_money", args = [sup.id]))
                #return HttpResponseRedirect("/")
            return show_message(request, "Ooopsz", "Valami baki lépett közbe, kérlek próbáld <a href=\"javascript:window.history.back();\">újra</a>!")
                
        return render_response(request, template, {
                                      'proj': proj,
                                      'form':form,
                                      'presents': suppres,
                                      'avg': avg,
                                      'non_pres':non_pres,
                                }, get_meta('/egy_otlet'))
    else:
        return HttpResponseRedirect(reverse("creativeselector.views.get_project", args = [proj.link]))

def send_money(request,pk):
    sup=get_object_or_404(TenderSupporter, pk = pk)    
    trans=CibPayTrans("uid"+str(request.user.id), "http://"+str(request.get_host())+reverse("creativeselector.views.send_money2", args = [pk]),sup.trid,sup.money)
    if 'is_english' in request.session and request.session['is_english']:
        trans.set_lang("EN")
    market_url="http://eki.cib.hu:8090/market.saki"
    cust_url="https://eki.cib.hu/customer.saki"
    msg1=str(trans.msg10())
    url1=market_url+'?'+msg1
    req=urllib2.Request(url=url1)
    rs=urllib2.urlopen(req)
    ans1c=rs.read()
    ans1=trans.answer11(ans1c)
    rs.close()
    taskqueue.add(url=reverse("creativeselector.views.erase_transaction", args = [pk,str(sup.trid)]), name=str(sup.trid), countdown=900)
    if ans1[3]=='00' :
        msg2=str(trans.msg20())
        url2=cust_url+'?'+msg2
        return HttpResponseRedirect(url2)
    else:
        return show_message(request, "Hibás az oldal", "Üzenet adatai: <br>msgt: %s<br>pid: %s<br>trid: %s<br>rc: %s<br>kérlek keresed fel az adminisztrátort" % (ans1[0],ans1[1],ans1[2],ans1[3]))
    
    #return show_message(request,"mi lehet a gond","nvn: %s<br/>nvn hossza: %s<br/>iv: %s<br/> iv hossza: %s<br/>" %(trans.nvn,len(trans.nvn),trans.iv,len(trans.iv)))

def send_money2(request,pk):    
    sup=None
    #sup=get_object_or_404(TenderSupporter, pk = pk)
    try:
        sup=TenderSupporter.objects.get(pk=pk)
    except:
        try:
            badtrs = BadTransaction.objects.get(supid = pk)
        except:
            badtrs = None
    if sup:
        trans=CibPayTrans("uid"+str(request.user.id), "http://"+str(request.get_host())+reverse("creativeselector.views.send_money2", args = [pk]),sup.trid,sup.money)
        template = 'present/back_from_shop.html'
        email_temp = "email/proj_sup_suporter"
        if 'is_english' in request.session and request.session['is_english']:
            trans.set_lang("EN")
            email_temp = "email/proj_sup_suporteren"
        market_url="http://eki.cib.hu:8090/market.saki"
        cust_url="https://eki.cib.hu/customer.saki"
        if request.method == "GET":
            data=str(request.get_full_path())
            datas=data.split('/')
            code=datas[3]
            code=code[1:]
            #return show_message(request,"mi lehet a gond","Kódolandó: %s<br/>" %(code))
            ans=trans.answer21(code)
        elif request.method == "POST":
            data=request.read()
            ans=trans.answer21(datas[2])
        else:
            ans=None
        #return show_message(request,"mi lehet a gond","What: %s<br/>" %(ans))
        ans2=[None,None,None,None,None,None,None]
        if ans[2]==sup.trid:
            msg1=str(trans.msg32())
            url1=market_url+'?'+msg1
            try:
                req=urllib2.Request(url=url1)
                rs=urllib2.urlopen(req)
                ans2c=rs.read()
                ans2=trans.answer31(ans2c)
                rs.close()
            except urllib2.HTTPError, e:
                msg1=str(trans.msg33())
                url1=market_url+'?'+msg1
                req=urllib2.Request(url=url1)
                rs=urllib2.urlopen(req)
                ans2c=rs.read()
                ans2=trans.answer31(ans2c)
                rs.close()
        else:
            return show_message(request,"Ooopsz hiba csúszott be","visszaérkező tömb adatai: %s<br/>eredeti trid: %s<br> ilyenkor kérjük keresse fel az adminisztrátort" %(ans, sup.trid))
        trid=ans2[2]
        rc=ans2[3]
        rt=ans2[4].decode('raw_unicode_escape')
        amo=ans2[6]
        anum=ans2[5]
        success=False
        try:
            projekt=Tender.objects.get(pk=sup.idea.id)
            if sup.pres:
                present=TenderPresent.objects.get(pk=sup.pres.id)
            else:
                present=None
        except:
            projekt=None
            present=None
        if ans2[3]=='00': #### pénzt zárolhatják
            sup.anum=ans2[5]
            sup.save()
            success=True
            pgurl='http://'+request.get_host()+reverse("creativeselector.views.get_project", args = [projekt.link])
            send_mail(u"admin@creativeselector.hu", projekt.owner.email, u"Támogatták a %s projektedet" % projekt.short_title ,
                  "email/proj_sup_owner", "email/proj_sup_owner",
                  { 'username' : u"%s %s" %(projekt.owner.last_name, projekt.owner.first_name), 'sup' : sup, 'pres': present,'url': u"http://%s/own_projekt/%s" % (request.get_host(),projekt.link),})        
            send_mail(u"admin@creativeselector.hu", sup.user.email, u"Köszöntünk támogatóink között - %s " % projekt.short_title ,
                  email_temp, email_temp,
                  { 'username' : sup.name, 'sup' : sup, 'pres': present, 'rc':rc, 'rt':rt,})
            send_mail(u"admin@creativeselector.hu", u"fizetes@creativeselector.hu", u"Tranzakció a(z) %s projekten" % projekt.short_title ,
                  "email/proj_sup_suporter", "email/proj_sup_suporter",
                  { 'username' : sup.name, 'sup' : sup, 'pres': present, 'rc':rc, 'rt':rt,})
            return render_response(request, template, {
                                      'trid': trid,
                                      'rc' : rc,
                                      'rt': rt,
                                      'anum': anum,
                                      'amo': amo,
                                      'projekt' : projekt,
                                      'success':success,
                                      'answer':ans2,
                                      'pageurl': pgurl,
                                      'is_admin': is_admin(request),
                                      }, get_meta('/egy_otlet'))  
        else: #### tranzakciós hiba
            badtrans=BadTransaction(
                idea=sup.idea,
                pres=sup.pres,
                supid=sup.pk,
                date=sup.date,
                money=sup.money,
                trid=sup.trid,
                name=sup.name,
                city=sup.city,
                rc=rc,
                rt=rt,
                zipcode=sup.zipcode,
                adress=sup.adress,
                tellno=sup.tellno
                )
            badtrans.save()
            send_mail(u"admin@creativeselector.hu", sup.user.email, u"Tranzakció sikertelen"  ,
                  email_temp, email_temp,
                  { 'username' : sup.name, 'sup' : sup, 'pres': present, 'rc':rc, 'rt':rt,})
            send_mail(u"admin@creativeselector.hu", u"fizetes@creativeselector.hu", u"Tranzakció a(z) %s projekten" % projekt.short_title ,
                  "email/proj_sup_suporter", "email/proj_sup_suporter",
                  { 'username' : sup.name, 'sup' : sup, 'pres': present, 'rc':rc, 'rt':rt,})
            return render_response(request, template, {
                                      'trid': trid,
                                      'rc':rc,
                                      'rt': rt,
                                      'anum': anum,
                                      'amo':amo,
                                      'projekt' : projekt,
                                      'success':success,
                                      'answer':ans2,
                                      'is_admin': is_admin(request),
                                }, get_meta('/egy_otlet'))
    elif badtrs:
        trans=CibPayTrans("uid"+str(request.user.id), "http://"+str(request.get_host())+reverse("creativeselector.views.send_money2", args = [pk]),badtrs.trid,badtrs.money)
        template = 'present/back_from_shop.html'
        email_temp = "email/proj_sup_suporter"
        if 'is_english' in request.session and request.session['is_english']:
            trans.set_lang("EN")
            email_temp = "email/proj_sup_suporteren"
        market_url="http://eki.cib.hu:8090/market.saki"
        cust_url="https://eki.cib.hu/customer.saki"
        if request.method == "GET":
            data=str(request.get_full_path())
            datas=data.split('/')
            code=datas[3]
            code=code[1:]
            #return show_message(request,"mi lehet a gond","Kódolandó: %s<br/>" %(code))
            ans=trans.answer21(code)
        elif request.method == "POST":
            data=request.read()
            ans=trans.answer21(datas[2])
        else:
            ans=None
        #return show_message(request,"mi lehet a gond","What: %s<br/>" %(ans))
        ans2=[None,None,None,None,None,None,None]
        msg1=str(trans.msg32())
        url1=market_url+'?'+msg1
        try:
            req=urllib2.Request(url=url1)
            rs=urllib2.urlopen(req)
            ans2c=rs.read()
            ans2=trans.answer31(ans2c)
            rs.close()
        except urllib2.HTTPError, e:
            msg1=str(trans.msg33())
            url1=market_url+'?'+msg1
            req=urllib2.Request(url=url1)
            rs=urllib2.urlopen(req)
            ans2c=rs.read()
            ans2=trans.answer31(ans2c)
            rs.close()
        trid=ans2[2]
        rc=ans2[3]
        rt=ans2[4].decode('raw_unicode_escape')
        amo=ans2[6]
        anum=ans2[5]
        success=False
        try:
            projekt=Tender.objects.get(pk=badtrs.idea.id)
            if badtrs.pres:
                present=TenderPresent.objects.get(pk=badtrs.pres.id)
            else:
                present=None
        except:
            projekt=None
            present=None
        if ans2[3]=='00': #### pénzt zárolhatják
            badtrs.anum=ans2[5]
            sup2=TenderSupporter(
                user=request.user,
                idea=badtrs.idea,
                pres=badtrs.pres,
                date=badtrs.date,
                money=badtrs.money,
                anum=anum,
                trid=trid,
                name=badtrs.name,
                city=badtrs.city,
                adress=badtrs.adress,
                tellno=badtrs.tellno,
                zipcode=badtrs.zipcode,
                )
            sup2.save()
            success=True
            pgurl='http://'+request.get_host()+reverse("creativeselector.views.get_project", args = [projekt.link])
            send_mail(u"admin@creativeselector.hu", projekt.owner.email, u"Támogatták a %s projektedet" % projekt.short_title ,
                  "email/proj_sup_owner", "email/proj_sup_owner",
                  { 'username' : u"%s %s" %(projekt.owner.last_name, projekt.owner.first_name), 'sup' : sup2, 'pres': present, 'url': u"http://%s/own_projekt/%s" % (request.get_host(),projekt.link),})        
            send_mail(u"admin@creativeselector.hu", sup2.user.email, u"Köszönjük, hogy támogattad a(z) %s projektet" % projekt.short_title ,
                  email_temp, email_temp,
                  { 'username' : sup2.name, 'sup' : sup2, 'pres': present, 'rc':rc, 'rt':rt,})
            send_mail(u"admin@creativeselector.hu", u"fizetes@creativeselector.hu", u"Tranzakció a(z) %s projekten" % projekt.short_title ,
                  "email/proj_sup_suporter", "email/proj_sup_suporter",
                  { 'username' : sup2.name, 'sup' : sup2, 'pres': present, 'rc':rc, 'rt':rt,})
            return render_response(request, template, {
                                      'trid': trid,
                                      'rc' : rc,
                                      'rt': rt,
                                      'anum': anum,
                                      'amo': amo,
                                      'projekt' : projekt,
                                      'success':success,
                                      'answer':ans2,
                                      'pageurl': pgurl,
                                      'is_admin': is_admin(request),
                                      }, get_meta('/egy_otlet'))  
        else: #### tranzakciós hiba
            badtrs.rc=rc
            badtrs.rt=rt
            badtrs.save()            
            return render_response(request, template, {
                                      'trid': trid,
                                      'rc':rc,
                                      'rt': rt,
                                      'anum': anum,
                                      'amo':amo,
                                      'projekt' : projekt,
                                      'success':success,
                                      'answer':ans2,
                                      'is_admin': is_admin(request),
                                }, get_meta('/egy_otlet'))
        return render_response(request, template, {
                                      'trid': badtrs.trid,
                                      'rc':badtrs.rc,
                                      'rt': badtrs.rt,
                                      'anum': None,
                                      'amo':badtrs.money,
                                      'projekt' : badtrs.idea,
                                      'success':False,
                                      'answer':'',
                                      'is_admin': is_admin(request),
                                }, get_meta('/egy_otlet'))
    else:
        return show_message(request,"Valami hiba lépett fel","Valami hiba lépett fel kérlek jelezd az adminisztrátornak" )
   
def erase_transaction(request,pk,trid):
    sup=get_object_or_404(TenderSupporter, pk = pk)
    try:
        btrs = BadTransaction.objects.get(trid = sup.trid)
    except:
        btrs = None
    #q = taskqueue.Queue()
    if not sup.anum:
        if not btrs:
            trans=CibPayTrans("uid"+str(request.user.id), "http://"+str(request.get_host())+reverse("creativeselector.views.send_money2", args = [pk]),sup.trid,sup.money)
            email_temp = "email/proj_sup_suporter"
            if 'is_english' in request.session and request.session['is_english']:
                trans.set_lang("EN")
                email_temp = "email/proj_sup_suporteren"
            market_url="http://eki.cib.hu:8090/market.saki"
            cust_url="https://eki.cib.hu/customer.saki"
            msg1=str(trans.msg32())
            url1=market_url+'?'+msg1
            try:
                req=urllib2.Request(url=url1)
                rs=urllib2.urlopen(req)
                ans1c=rs.read()
                ans1=trans.answer31(ans1c)
                rs.close()
            except urllib2.HTTPError, e:
                msg2=str(trans.msg33())
                url2=market_url+'?'+msg2
                try:
                    req=urllib2.Request(url=url2)
                    rs=urllib2.urlopen(req)
                    ans2c=rs.read()
                    ans1=trans.answer31(ans2c)
                    rs.close()
                    ans1[4]=ans1[4].decode('raw_unicode_escape')
                    send_mail(u"admin@creativeselector.hu", sup.user.email, u"Tranzakció időtúllépés miatt sikertelen"  ,
                              email_temp, email_temp,
                              { 'username' : sup.name, 'sup' : sup, 'pres': None, 'rc':ans1[3], 'rt':ans1[4],})
                    send_mail(u"admin@creativeselector.hu", u"fizetes@creativeselector.hu", u"Tranzakció  időtúllépés miatt sikertelen"  ,
                              "email/proj_sup_suporter", "email/proj_sup_suporter",
                              { 'username' : sup.name, 'sup' : sup, 'pres': None, 'rc':ans1[3], 'rt':ans1[4],})
                except urllib2.HTTPError, e:
                    rc=e.read()
                    rc=rc[3:]
                    ans1=[None,None,None,rc,'Időtúllépés']
                    send_mail(u"admin@creativeselector.hu", sup.user.email, u"Tranzakció időtúllépés miatt sikertelen"  ,
                              email_temp, email_temp,
                              { 'username' : sup.name, 'sup' : sup, 'pres': None, 'rc':ans1[3], 'rt':ans1[4],})
                    send_mail(u"admin@creativeselector.hu", u"fizetes@creativeselector.hu", u"Tranzakció  időtúllépés miatt sikertelen"  ,
                              "email/proj_sup_suporter", "email/proj_sup_suporter",
                              { 'username' : sup.name, 'sup' : sup, 'pres': None, 'rc':ans1[3], 'rt':ans1[4],})
            except:
                ans1=[None,None,None,None,None]            
            if ans1[3]=='00':
                sup2=get_object_or_404(TenderSupporter, pk = pk)
                if not sup2.anum:
                    badtrans=BadTransaction(
                        idea=sup2.idea,
                        pres=sup2.pres,
                        supid=sup2.pk,
                        date=sup2.date,
                        money=sup2.money,
                        trid=sup2.trid,
                        name=sup2.name,
                        city=sup2.city,
                        rc=ans1[3],
                        rt=ans1[4],
                        zipcode=sup2.zipcode,
                        adress=sup2.adress,
                        tellno=sup2.tellno
                        )
                    badtrans.save()
                    sup2.delete()
            else:
                badtrans=BadTransaction(
                    idea=sup.idea,
                    pres=sup.pres,
                    supid=sup.pk,
                    date=sup.date,
                    money=sup.money,
                    trid=sup.trid,
                    name=sup.name,
                    city=sup.city,
                    rc=ans1[3],
                    rt=ans1[4],
                    zipcode=sup.zipcode,
                    adress=sup.adress,
                    tellno=sup.tellno
                    )
                badtrans.save()
                sup.delete()
            #taskqueue.delete_tasks(taskqueue.Task(name=trid))
            return show_message(request, "Hibás az oldal", "Üzenet adatai: %s<br>" % (ans1))
        else:
            sup.delete()
            return show_message(request, "minden ok", "Minden ok" )
    else:
        #taskqueue.delete_tasks(taskqueue.Task(name=trid))
        return show_message(request, "minden ok", "Minden ok" )

###################################
# IMPORT RSS
###################################

def strip_pingback(content):
    content = re.compile(u'<a href="http://creativeselector.posterous.com/.*">.*<\/a>').sub(u'', content)
    #content = re.compile(u'<span>\&nbsp\;<\/span>').sub(u'', content)
    #content = re.compile(u'<p>\&nbsp\;<\/p>').sub(u'', content)
    #content = re.compile(u'<p>\w*<\/p>').sub(u'', content)
    #content = re.compile(u'<span>\w*<\/span>').sub(u'', content)
    content = content.replace("|", "")
    return content.strip()

def create_summary(content):
    return striptags(truncatewords_html(content, 20))

                                           
def import_rss(request):
  #for i in range(1,10):
    rss_link = settings.RSS_SOURCE % 1
    result = urlfetch.fetch(rss_link, deadline=55)
    if result.status_code == 200:
      website = result.content
      d = feedparser.parse(website)
      for e in d.entries:
        logging.debug(e)

        up = e.updated_parsed
        created = datetime(up[0], up[1], up[2], up[3], up[4], up[5], up[6]) 
        
        if not e.title.startswith(u'Új ötlet') and not e.title.startswith(u'Módosított ötlet') :
          # van e mar ilyen hir
          try:
            #logging.error("RSS title = %s" % e.title)
            
            existing_blog = BlogEntry.objects.get(title = e.title)
            existing_blog.content=strip_pingback(e.description)
            existing_blog.save()
          #existing_blog = BlogEntry.objects.get(created = created)
          except BlogEntry.DoesNotExist:
            #logging.error("Add New BlogEntry with title = %s" % e.title)
            #ha nincs
            blog = BlogEntry(title = e.title, content  =  strip_pingback(e.description), created = created, published = True)
            blog.save()
            #logging.error("New BlogEntry added with title = %s" % blog.title)
          except:
            pass

    return render_response(request, 'facebook/show_string.html', {
                                'string': "import was successfull!"
                          }, get_meta(''))

###################################
# EXCEL EXPORT
###################################

import xlwt     
from xlwt import *       

def flat_url(string):
  return string.replace("<a href=\'", "").replace("\'>"," (").replace("</a><br>", ")")              

def write_header(sheet, fields):
  # Create header
  boldFont = Formatting.Font()
  boldFont.name = 'Arial'
  boldFont.bold = True
  st0 = XFStyle()
  st0.font = boldFont
  
  i = 0
  for field in fields:
    sheet.write(0, i, field, st0)
    sheet.col(i).width = 0x0d00 + len(field) * 100
    i += 1

# create styles
f = Font()
f.name = 'Arial'
f.underline = Font.UNDERLINE_SINGLE
f.colour_index = 4

h_style = XFStyle()
h_style.font = f

o_style = XFStyle()
    
def write_one_user(sheet, excel_export, row):
    
  sheet.write(row, 0, excel_export.userid, o_style)
  sheet.write(row, 1, excel_export.facebook_id, o_style)
  sheet.write(row, 2, excel_export.username, o_style)
  sheet.write(row, 3, excel_export.first_name, o_style)
  sheet.write(row, 4, excel_export.last_name, o_style)
  sheet.write(row, 5, excel_export.email, o_style)
  sheet.write(row, 6, excel_export.own_idea_count, o_style)
  sheet.write(row, 7, flat_url(excel_export.own_idea), h_style)
  sheet.write(row, 8, excel_export.supported_idea_count, o_style)
  sheet.write(row, 9, flat_url(excel_export.supported_idea), h_style)

def write_one_user_new(sheet, excel_export, row):
    
  sheet.write(row, 0, excel_export.username, o_style)
  sheet.write(row, 1, excel_export.first_name, o_style)
  sheet.write(row, 2, excel_export.last_name, o_style)
  sheet.write(row, 3, excel_export.email, o_style)

from google.appengine.api.labs import taskqueue

def send_mail_ready(request, mode):
  to_email ="admin@creativeselector.hu"
  subject = u"Kivonat elkeszult"
  message = u"http://%s/admin/creativeselector/excelexport/" % (request.get_host())
  
  if mode == "1":
    message = "ennyi"
    subject = "Hasonlo otletek frissitese elkeszult"
  if settings.DEBUG:
    to_email = "opitzer@gmail.com"
    #clixposite@gmail.com
  send_mail("clixposite@gmail.com", to_email, subject, "email/message_email", "email/message_email_html", {'message': message})
    
  return HttpResponse("Task Executed", mimetype='text/plain')    

from tagging.models import ModelTags, Tag, TaggedItem

def refresh_related_items(request):
  ModelTags.objects.all().delete()
  Tag.objects.all().delete()
  TaggedItem.objects.all().delete()
  
  tenders = Tender.objects.all()

  for tender in tenders:
    taskqueue.add(url='/refresh_one_related_item/%s' % tender.id, params={})
  
  #taskqueue.add(url='/send_mail_ready/1')
  return HttpResponseRedirect("/admin/")   
  
def refresh_one_related_item(request, id):
  tender = Tender.objects.get(id = id)
  
  tender.generate_tags()
  return HttpResponse("Task Executed", mimetype='text/plain')

USER_EXPORT_LIMIT = 1000

def export_users(request):
  all_user_count = User.objects.count()  
  modulo_user_count = all_user_count/USER_EXPORT_LIMIT;

  logging.error("modulo_user_count %s" % modulo_user_count)   
  for i in range(0, modulo_user_count+1):
    from_limit = i * USER_EXPORT_LIMIT
    to_limit = min((i+1) *USER_EXPORT_LIMIT, all_user_count) 
    is_last = int(i >= modulo_user_count)
    logging.error("Add task batch with url /export_users_batch/%s/%s/%s" % (from_limit, to_limit, is_last))
    taskqueue.add(url='/export_users_batch/%s/%s/%s' % (from_limit, to_limit, is_last), params={})
  
  return HttpResponseRedirect("/admin/") 
  
def export_users_batch(request, from_limit, to_limit, last):
  logging.error("export_users_batch limits = %s, %s, %s" % (from_limit, to_limit, last))
  users = User.objects.all()[from_limit:to_limit]
  logging.error("export_users_batch users = %s" % len(users))

  for user in users:
    #logging.error("Add task with url /export_users_task/%s" % user.id)
    taskqueue.add(url='/export_users_task/%s' % user.id, params={})

  if last:
    taskqueue.add(url='/send_mail_ready/0')
  return HttpResponseRedirect("/admin/")   
    
def export_users_task(request, user_id):  
  u = User.objects.get(id=user_id)
  try:
    e = ExcelExport.objects.get(userid = user_id)
    e.delete()
  except:
    pass
  own_ideas = u.tender_set.all()
  supported_ideas = u.tendersupporter_set.all()
  
  own_tenders = None
  for own_tender in own_ideas:
    if not own_tenders:
      own_tenders = "<a href='http://www.creativeselector.hu%s'>%s</a><br/>" %  (own_tender.get_absolute_url(), own_tender.short_title)
    else:
      own_tenders = "%s\n<a href='http://www.creativeselector.hu%s'>%s</a><br/>" %  (own_tenders, own_tender.get_absolute_url(), own_tender.short_title)
  

  supported_tenders = None
  try:
    for supported_tender in supported_ideas:
      if not supported_tenders:
        supported_tenders = "<a href='http://www.creativeselector.hu%s'>%s</a><br/>" %  (supported_tender.idea.get_absolute_url(), supported_tender.idea.short_title)
      else:
        supported_tenders = "%s\n<a href='http://www.creativeselector.hu%s'>%s</a><br/>" % (supported_tenders, supported_tender.idea.get_absolute_url(), supported_tender.idea.short_title)
  except:
    pass

  facebook_id = -1
  try:
    facebook_id = u.facebook_profile.facebook_id
  except:
    pass
  
  e = ExcelExport(
    userid = u.id,
    facebook_id = facebook_id,
    username = u.username,
    first_name = u.first_name,
    last_name = u.last_name,
    email = u.email,
    own_idea_count = len(own_ideas),
    idea_owner = (len(own_ideas) > 0),
    own_idea = own_tenders,
    supported_idea_count = len(supported_ideas),
    supporter = (len(supported_ideas) > 0),
    supported_idea = supported_tenders)
  
  e.save()
  return HttpResponse("Task Executed", mimetype='text/plain')  
    
"""
def names_to_excel(request):
    if is_admin(request):
        names = NameExport.objects.all()
        book = xlwt.Workbook(encoding='utf-8')
        all_users_sheet = book.add_sheet('Minden felhasználó')
        write_header(all_users_sheet, [u"Vezetéknév", u"Keresztnév", u"Email"])
        i=1
        for nm in names:
            all_users_sheet.write(i, 0, nm.last_name, o_style)
            all_users_sheet.write(i, 1, nm.first_name, o_style)
            all_users_sheet.write(i, 2, nm.email, o_style)
            i+=1
        data = book.save('cs_users.csv')
        response = HttpResponse(book.save('cs_users.csv'), mimetype='text/csv')                                     
        response['Content-Disposition'] = 'attachment; filename=cs_users.csv'
        return response
    else:
        return HttpResponseRedirect("/")
"""
def excel_export(request):
  book = xlwt.Workbook(encoding='utf-8')

  all_users_sheet = book.add_sheet('Minden felhasználó')
  idea_owners_sheet = book.add_sheet('Ötletgazdák')
  supporters_sheet = book.add_sheet('Támogatók')
  others_sheet = book.add_sheet('Egyéb')
  

  write_header(all_users_sheet, [u"Azonosító", u"Facebook azonosító", u"Felh. név", u"Keresztnév", u"Vezetéknév", u"Email", u"Saját ötletek száma", u"Saját ötletek", u"Támogatott ötletek száma", u"Támogatott ötletek"])

  write_header(idea_owners_sheet, [u"Azonosító", u"Facebook azonosító", u"Felh. név", u"Keresztnév", u"Vezetéknév", u"Email", u"Saját ötletek száma", u"Saját ötletek", u"Támogatott ötletek száma", u"Támogatott ötletek"])

  write_header(supporters_sheet, [u"Azonosító", u"Facebook azonosító", u"Felh. név", u"Keresztnév", u"Vezetéknév", u"Email", u"Saját ötletek száma", u"Saját ötletek", u"Támogatott ötletek száma", u"Támogatott ötletek"])

  write_header(others_sheet, [u"Azonosító", u"Facebook azonosító", u"Felh. név", u"Keresztnév", u"Vezetéknév", u"Email", u"Saját ötletek száma", u"Saját ötletek", u"Támogatott ötletek száma", u"Támogatott ötletek"])

  # List all users
  excel_exports = ExcelExport.objects.all()
  i1 = i2 = i3 = i4 = 1
  for excel_export in excel_exports:
    write_one_user(all_users_sheet, excel_export, i1)
    
    if excel_export.idea_owner:
      write_one_user(idea_owners_sheet, excel_export, i2)
      i2 += 1

    if excel_export.supporter:
      write_one_user(supporters_sheet, excel_export, i3)
      i3 += 1
      
    if excel_export.supporter and excel_export.idea_owner:
      write_one_user(others_sheet, excel_export, i4)
      i4 += 1
    
    i1 += 1

  data = book.save('cs_allusers.xls')
   
  response = HttpResponse(book.save('cs_allusers.xls'), mimetype="application/vnd.ms-excel")                                     
  response['Content-Disposition'] = 'attachment; filename=cs_allusers.xls'
  
  return response

def ideauser_get(request):
  
  ideauser = Tender.objects.all()
  return render_response(request, 'idea/idea_owners.html', {
                                'ideas': ideauser,                                
                          }, get_meta('/otletek'))

def user_get(request):
    users=FacebookProfile.objects.all()
    return render_response(request, 'dijatado_2011.html', {
                                'users': users,                                
                          }, get_meta('/otletek'))

def deleteuser(request):
    usr = request.user
    tnds = None
    usrd = None
    #own_tenders = None
    try:
        tnds = TenderSupporter.objects.all().filter(user = usr)
    except:
        tnds = None
    for tender in tnds:
        if(tender):
            tender.delete()
    #try:
       # own_tenders = Tender.objects.filter(owner = request.user)
       # own_tenders.delete()
    #except:
     #   own_tenders = None
    try:
        usrd = User.objects.get(id=usr.id)
        usrd.delete()
    except:
        usrd = None
    return render_response(request, 'profil_del.html', {
                                  'usr': usr,
                                  'usrd': usrd,
                                  'logged_in': check_user(request),
                            }, get_meta('/profil'))

def deluser(request):
    return render_response(request, 'profil_del_first.html', {                                
                          }, get_meta('/profil'))

###################################
# NECC
###################################      
"""
def necc_category(request, category_link):     
  necc_category = u"Összes"         
  necc_companies = None         
  request.session['uploaded'] = None
  
  if category_link == "uj_otletek":
    necc_category = u"Új ötletek"
    necc_companies =  NeccCompany.objects.all().filter(new_idea = True, enabled = True).order_by("-created")[:5]
  elif category_link == "ujonann_finanszirozott":
    necc_category = u"Újonnan finanszírozott"
    necc_companies =  NeccCompany.objects.all().filter(newly_founded = True, enabled = True).order_by("-created")[:5]
  elif category_link == "ujonann_erkezett":
    necc_category = u"Újonnan érkezett"
    necc_companies =  NeccCompany.objects.all().filter(enabled = True).order_by("-created")[:5]
  elif category_link:
    necc_category = NeccCategory.objects.get(link = category_link)
    necc_companies =  NeccCompany.objects.filter(category = necc_category, enabled = True)
  else:
    necc_companies =  NeccCompany.objects.all().filter(enabled = True)
  
  necc_categories = NeccCategory.objects.all().order_by('link')
  
  return render_response(request, 'necc/necc_category.html', {
                          'necc_companies': necc_companies,
                          'necc_category': necc_category,
                          'necc_categories': necc_categories,
                    }, get_meta(''))

def necc_category_all(request):
  request.session['uploaded'] = None
  return necc_category(request, None)

def necc_search(request, key):
  request.session['uploaded'] = None
  all_companies =  NeccCompany.objects.all()
  necc_companies = [i for i in all_companies if key.lower() in i.companyname.lower()] #"%s%s%s" % (i.companyname.lower(), i.website.lower(), i.description.lower())]
  necc_categories = NeccCategory.objects.all().order_by('link')
  necc_category = u"Keresés: %s" % key 
  return render_response(request, 'necc/necc_category.html', {
                          'necc_companies': necc_companies,
                          'necc_category': necc_category,
                          'necc_categories': necc_categories
                    }, get_meta(''))

  
  """
###################################
# PRIVATE UTILITIES
###################################
def update_users(request):
    tenders = Tender.objects.all()
    user = User.objects.get(id=10)
    for tender in tenders:
      tender.owner = user
      tender.save()

"""
def update_users(request):
    request.facebook = partial(_facebook, request)
    facebook_user = request.facebook()
    profiles = FacebookProfile.objects.all()
    for profile in profiles:
      try:
        if not profile.name:
          fb_profile = facebook_user.get_object(profile.facebook_id)
          profile.name = fb_profile['name']
          profile.image = 'https://graph.facebook.com/%s/picture?type=large' % profile.facebook_id
          profile.thumb = 'https://graph.facebook.com/%s/picture' % profile.facebook_id
          profile.profile_link = fb_profile['link']
          profile.save()
      except:
        pass
    return render_response(request, 'examples.html', {
                                
                          }, get_meta(''))
"""                          
def flush_memcache(request):
  from google.appengine.api import memcache
  memcache.flush_all()
  stats = memcache.get_stats()
  return render_response(request, 'facebook/show_string.html', {
                              'string': stats
                        }, get_meta(''))         
                                    

"""
def write_to_xml(request):
    import django.db.models
    models = django.db.models.get_models()
    model = BlogEntry.objects.all()
    writer = XMLWriter(pretty=False)
    writer.open("djangoexport")
    for item in model:
        #writer.open(model._meta.object_name)
        for field in item._meta.fields:
            writer.open(field.name)
            try:
                value = getattr(item, field.name)
                if value != None:
                    if isinstance(value, django.db.models.base.Model):
                        # This field is a foreign key, so save the primary key
                        # of the referring object
                        pk_name = value._meta.pk.name
                        pk_value = getattr(value, pk_name)
                        writer.content(pk_value)
                    else:
                        writer.content(value)
                writer.close()
            except:
                writer.close()
        writer.close()
    writer.close()
    #writer.save("export_bubble_text.xml")
    #response = HttpResponse(writer.save("export_bubble_text.xml"))
    #response['Content-Disposition'] = 'attachment; filename=export_bubble_text.xml'
    out = writer.output

    return render_response(request, 'xml.html', {
                                'output': out,                                
                          }, get_meta('/otletek'))

"""
