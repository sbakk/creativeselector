DEBUG = true;
// ##############################################
// Logging
function log(value) {
  if (DEBUG) {
      jQuery.log(value);
  }
}

// ##############################################
// Jump
function jump(link) {
  document.location.href=link;
}

// ##############################################
// Core post function

function showSpinner(id) {
  
}

function hideSpinner(id) {
  
}


function post(url, form_id, div_id) {
  if (arguments.length == 2) {
    div_id = "content";
  }
  log("form = " +f(form_id))
  showSpinner(div_id);
  $.post(url, $(f(form_id)).serialize(), function(data){
     $(f(div_id)).html(data);
     hideSpinner(div_id);
  }); 
}

// ##############################################
// Core get function
function _get(url, div_id, async) {
  showSpinner(div_id);
  
  $.ajax({
    url: url,
    async: async,
    dataType: "text",
    success: function (data) {
      $(f(div_id)).html(data);
      hideSpinner(div_id);
    }
  });
}

// ##############################################
// Async get function
function get(url, div_id) {
  _get(url, div_id, true);
}

// Sync get function
function getsync(url, div_id) {
  _get(url, div_id, false);
}

// ##############################################
// If document has loaded

var USERNAME_PLACEHOLDER = "felhasználónév"
var PASSWORD_PLACEHOLDER = "jelszó"

$(function() {
	$('#gallery a').lightBox({fixedNavigation:true});
});

$(document).ready(function() {
  Cufon.replace('.cufon');
  Cufon.replace('.cufon2', {letterSpacing: '-0.5px'});

	
	$.imgpreload([
	    '/media/images/btn_ideas.png',
      '/media/images/btn_send_ideas.png',
      '/media/images/btn_profile.png',
      '/media/images/btn_info.png',
      '/media/images/btn_blog.png',
      '/media/images/btn_faq.png',
      '/media/images/btn_necc2.png',
      '/media/images/btn_projektek.png',
      '/media/images/btn_ideas_over.png',
      '/media/images/btn_send_ideas_over.png',
      '/media/images/btn_profile_over.png',
      '/media/images/btn_info_over.png',
      '/media/images/btn_blog_over.png',
      '/media/images/btn_faq_over.png',
      '/media/images/btn_necc_over2.png',
      '/media/images/btn_projektek_over.png'
      
	    ],function()
  {
 
  });
  
  // countdown
  $('#counter_holder').countdown({
    until: new Date(2010, 10 - 1, 25),
    tickInterval: 60,
    layout: '<div id="days" class="unit">{dnn}</div><div id="hours" class="unit">{hnn}</div> <div id="minutes" class="unit">{mnn}</div>'
  }); 
  

  
});


// ###################################################
// ADD BUBBLE

function addBubble(id, html) {

  if ($.exists(f(id))) {
    $(f(id)).SetBubblePopup({
    								innerHtml: html, 
    								bubbleAlign: 'left',
    								tailAlign: 'left',
    								color: 'grey',
    								contentStyle: 'font-size:14px;',
    								imageFolder: '/media/images/bp_images'

    });
  }
}
