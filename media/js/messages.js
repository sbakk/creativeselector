

jQuery.exists = function(selector) {return ($(selector).length);}

// ##############################################
// Extends string with beginWith function
String.prototype.beginsWith = function(t, i) {
  if (i==false) {
    return (t == this.substring(0, t.length));
  } else {
    return (t.toLowerCase() == this.substring(0, t.length).toLowerCase());
  }
} 

String.prototype.endsWith = function(str)
{
    var lastIndex = this.lastIndexOf(str);
    return (lastIndex != -1) && (lastIndex + str.length == this.length);
}


// ##############################################
// Appends hashmark to get a jquery id
function f(value) {
  return '#' + value;
}

// ##############################################
// Gets field by form and field id
function gf(form_id, field_id) {
  field = $(f(form_id) +" input[name="+field_id+"]");
  
  //log("input = "+f(form_id) +" input[name="+field_id+"] ---" +field);
  
  if (!$.exists(field)) {
    field = $(f(form_id) +" textarea[name="+field_id+"]");
    //log("input = "+f(form_id) +" textarea[name="+field_id+"] ---" +field);
  }
  
  if (!$.exists(field)) {
    field = $(f(form_id) +" select[name="+field_id+"]");
    //log("input = "+f(form_id) +" select[name="+field_id+"] ---" +field);
  }
  
  if (!$.exists(field)) {
    field = $(f(field_id));
    //log("input = "+f(form_id) +" select[name="+field_id+"] ---" +field);
  }
  return field
}

// ##############################################
// Gets field value from a form
function gv(form_id, field_id) {
  return gf(form_id, field_id).val();
}

var nameRegex = /^[a-zA-Z]+(([\_][a-zA-Z])?[a-zA-Z0-9]*)*$/;
var emailRegex = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;
var messageRegex = new RegExp(/<\/?\w+((\s+\w+(\s*=\s*(?:".*?"|'.*?'|[^'">\s]+))?)+\s*|\s*)\/?>/gim);

// START OF MESSAGE SCRIPT //

var MSGTIMER = 20;
var MSGSPEED = 5;
var MSGOFFSET = 3;
var MSGHIDE = 3;


function resetForm(form) {
  $("input").each(function() {
     $(this).removeClass("errorBorder");
  });

  $("textarea").each(function() {
     $(this).removeClass("errorBorder");
  });

  $(".msg").each(function() {
     $(this).hide();
  });
}

function em(form_id, field_id, string) {
   setTimeout("im('"+form_id+"', '"+field_id+"', '"+string+"')", 500);
}

function im(form_id, field_id, string) {
  inlineMsg(form_id, field_id, string, 2);
}

function is_empty(form_id, id, check_length) {
  if (arguments.length == 2) {
    check_length = true;
  }

  
  var value = gv(form_id, id);
  
  if (jQuery.trim(value) == "") {
    im(form_id, id, 'Kötelező mező.');
    return false;
  }
  
  if (check_length) {
    return min_length(form_id, id, 4);
  }
  return true;
}

function word_validator(form_id, id, min, max) {
  var value = gv(form_id, id);
  var count = value.split(' ').length;
  
  if (count < min) {
    im(form_id, id, 'Túl rövid! Minimum '+min+' szót kell írnod.');
    return false;
  }

  if (count > max) {
    im(form_id, id, 'Túl hosszú! Maximum '+max+' szóban fejtsd ki.');
    return false;
  }
  return true;
}

function min_length(form_id, id, len) {
  var value = gv(form_id, id);
  
  if (jQuery.trim(value).length < len) {
    im(form_id, id, 'Minimum hosszúság '+len+' karakter.');
    return false;
  }
  
  return true;
}

function is_not_email(form_id, id) {
  var value = gv(form_id, id);
  
  if (!value.match(emailRegex)) {
    im(form_id, id, 'Adj meg egy valós email címet!');
    return false;
  }
  
  return true;
}

function is_not_name(form_id, id) {
  var value = gv(form_id, id);

  
  if (!value.match(nameRegex)) {
    im(form_id, id, 'A felhasználónév csak az angol ABC betűit tartalmazhatja, és számokat vagy alulvonást.');
    return false;
  }
  
  return true;
}

function is_not_same(form_id, id, id2) {
  var value = gv(form_id, id);
  var value2 = gv(form_id, id2);
  
  if (value != value2) {
     im(form_id, id2, 'Ismételd meg a jelszavadat.');
     return false;
   }

   return true;
}

// build out the divs, set attributes and call the fade function //
function inlineMsg(form_id, field_id, string, autohide) {
  var msg;
  var msgcontent;

  var divID = 'msg'+form_id +field_id;
  
  if(!$.exists(f(divID))) {
    msg = $(document.createElement("div"));
    msg.attr('id', divID);
    
    msgcontent = $(document.createElement("div"));
    msgcontent.attr('id', 'msgcontent'+form_id +field_id);
    
    $('body').append(msg);
    msg.append(msgcontent);
    msg.hide();
  } else {
    msg = $(f(divID));
    msgcontent = $(f('msgcontent'+form_id+field_id));
  }

  msg.addClass("msg");
  msgcontent.addClass("msgcontent");
  
  msgcontent.html(string);

  var msgheight = msg.height();
  var targetdiv = gf(form_id, field_id);
  
  targetdiv.addClass("errorBorder");

  var targetheight = targetdiv.height();
  var targetwidth = targetdiv.width();
  var topposition = targetdiv.offset().top - ((msgheight - targetheight) / 2) +2;
  var leftposition = targetdiv.offset().left + targetwidth + 5;

  log("leftposition = "+topposition+", leftposition  = " + leftposition);
  msg.css('top', topposition + 'px');
  msg.css('left',  leftposition + 'px');
  msg.fadeIn();
}

// preload the arrow //
if(document.images) {
  arrow = new Image(7,80); 
  arrow.src = "/media/images/msg_arrow.gif"; 
}