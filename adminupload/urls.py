from django.conf.urls.defaults import *

urlpatterns = patterns('adminupload.views',
    (r'^reupload/(?P<pk>.+)/(?P<model_info>.+)/(?P<field_name>.+)/$', 'reupload_handler'),
    (r'^reupload_inline/(?P<pk>.+)/(?P<inline_model_info>.+)/(?P<model_info>.+)/(?P<field_name>.+)/$', 'reupload_handler_inline'),
    (r'^reupload_inline_single/(?P<pk>.+)/(?P<inline_model_info>.+)/(?P<model_info>.+)/(?P<field_name>.+)/$', 'reupload_inline_single'),
    (r'^close_popup$', 'close_popup'),
    (r'^download/(?P<pk>.+)/(?P<model_info>.+)/(?P<field_name>.+)/$', 'download_handler'),
)
