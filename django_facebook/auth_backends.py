from django.contrib.auth import models, backends
from creativeselector.models import FacebookProfile
from django.db.models.query_utils import Q
import logging

class FacebookBackend(backends.ModelBackend):
    def authenticate(self, facebook_id=None, facebook_email=None):
        '''
        Authenticate the facebook user by id OR facebook_email
        '''
        logging.debug("FACEBOOK_ID = %s" % facebook_id)
        profiles = FacebookProfile.objects.filter(facebook_id=facebook_id)[:1]
        logging.debug("PROFILES = %s" % profiles)
        if profiles:
            user = profiles[0].user
            return user



