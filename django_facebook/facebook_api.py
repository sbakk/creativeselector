from django_facebook.facebook import GraphAPI, GraphAPIError
from django.core.mail import send_mail, mail_admins
from django_facebook import settings as facebook_settings
import datetime
from django.forms.util import ValidationError
import logging

class FacebookAPI(GraphAPI):
    '''
    Wrapper around the default facebook api with
    - support for creating django users
    - caches registration and profile data, ensuring
    efficient use of facebook connections
    '''
    def __init__(self, user, timeout=30):
        self._is_authenticated = None
        self._profile = None
        self.access_token = user['access_token'] if user else None 
        self.timeout = timeout
        if user:
            GraphAPI.__init__(self, self.access_token)


    def is_authenticated(self):
        '''
        Checks if the cookie/post data provided is actually valid
        '''
        if self._is_authenticated is None:
            try:
                self.facebook_profile_data()
                self._is_authenticated = True
            except GraphAPIError, e:
                self._is_authenticated = False

        return self._is_authenticated


    def facebook_profile_data(self):
        '''
        Returns the facebook profile data, together with the image locations
        '''
        t=[None,None]
        if self._profile is None:
            t=self.get_object('me')
            logging.error(t)
            profile = t #profile = t[1]
            if profile and "id" in profile:
              profile['image'] = 'https://graph.facebook.com/%s/picture' % profile['id']
              profile['image_thumb'] = 'https://graph.facebook.com/%s/picture' % profile['id']
            self._profile = profile
        return self._profile


    def facebook_registration_data(self):
        '''
        Gets all registration data
        and ensures its correct input for a django registration
        '''
        if self.is_authenticated():
            facebook_profile_data = self.facebook_profile_data()
            user_data = None
            try:
                user_data = FacebookAPI._convert_facebook_data(facebook_profile_data)
            except Exception, e:
                logging.error("Broken Facebook profile")
                pass

        return user_data

    @classmethod
    def _convert_facebook_data(cls, facebook_profile_data):
        '''
        Takes facebook user data and converts it to a format for usage with Django
        '''
        profile = facebook_profile_data.copy()
        user_data = facebook_profile_data.copy()

        user_data['username'] = cls._username_slugify(u"%s %s" % (profile.get('name'), profile.get('id')))
        user_data['facebook_profile_url'] = profile.get('link')
        user_data['facebook_name'] = profile.get('name')
        user_data['first_name'] = profile.get('first_name')
        user_data['last_name'] = profile.get('last_name')
        user_data['id'] = profile.get('id')
        try:
          user_data['email'] = profile.get('email')
        except:
          user_data['email'] = ""

        logging.error("USERNAME = %s", user_data['username'])

        return user_data


    @classmethod
    def _username_slugify(cls, username):
        '''
        Slugify the username and replace - with _ to meet username requirements
        '''
        from django.template.defaultfilters import slugify
        return slugify(username).replace('-', '_')
