#!/usr/bin/env python

from django.core.management import setup_environ
import settings
setup_environ(settings)
from creativeselector.models import *
from datetime import datetime
import sys
from django.contrib.auth.models import User

import time

def load_users():
  User.objects.all().delete()
  #FacebookProfile.objects.all().delete()
  # SAVE COUNTRIES
  for line in open('tools/cs_allusers.csv','r').readlines():
    line = line.strip()
    arr = line.split("#")
    user = User(id=arr[0], username = arr[2], first_name=arr[3], last_name=arr[4], email = arr[5])
    user.save()
    
    fp = FacebookProfile(user_id = arr[0], facebook_id = arr[1])
    fp.save()
  

def main(argv):  
  load_users()
    
if __name__ == "__main__":
    main(sys.argv[1:])