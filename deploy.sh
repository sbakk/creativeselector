#####################
# app.yaml changes
#####################

#python manage.py generatemedia

# FACEBOOK LOCAL KEYS
LOCAL_FACEBOOK_API_KEY="acf6e2957783c62ba01756efadb80559"
LOCAL_FACEBOOK_SECRET_KEY="276a2b12723e25e2aceeeb021d34b340"

# FACEBOOK LIVE KEYS
LIVE_FACEBOOK_API_KEY="b8b01d46e1d7f4850ca74984e3201978"
LIVE_FACEBOOK_SECRET_KEY="1a4cd090970032a7bd43ccd93038aac3"

# URLS
LOCAL_URL="localhost\:8080"
LIVE_URL="www.creativeselector.hu"

sed 's/FACEBOOK_API_KEY = \"'$LOCAL_FACEBOOK_API_KEY'\"/FACEBOOK_API_KEY = \"'$LIVE_FACEBOOK_API_KEY'\"/g' settings.py >  settings.py.tmp

sed 's/FACEBOOK_SECRET_KEY = \"'$LOCAL_FACEBOOK_SECRET_KEY'\"/FACEBOOK_SECRET_KEY = \"'$LIVE_FACEBOOK_SECRET_KEY'\"/g' settings.py.tmp >  settings.py.tmp2

sed 's/'$LOCAL_URL'/'$LIVE_URL'/g' settings.py.tmp2 >  settings.py.tmp3

mv settings.py.tmp3 settings.py
rm -f settings.py.tmp
rm -f settings.py.tmp2

cp appcfg_custom.py /Applications/GoogleAppEngineLauncher.app/Contents/Resources/GoogleAppEngine-default.bundle/Contents/Resources/google_appengine/google/appengine/tools/appcfg.py

#####################
# deploy
#####################
python manage.py deploy 

#####################
# app.yaml changes back
#####################

cp appcfg.py /Applications/GoogleAppEngineLauncher.app/Contents/Resources/GoogleAppEngine-default.bundle/Contents/Resources/google_appengine/google/appengine/tools/appcfg.py

######################
# facebook keys for local testing
######################
sed 's/FACEBOOK_API_KEY = \"'$LIVE_FACEBOOK_API_KEY'\"/FACEBOOK_API_KEY = \"'$LOCAL_FACEBOOK_API_KEY'\"/g' settings.py >  settings.py.tmp

sed 's/FACEBOOK_SECRET_KEY = \"'$LIVE_FACEBOOK_SECRET_KEY'\"/FACEBOOK_SECRET_KEY = \"'$LOCAL_FACEBOOK_SECRET_KEY'\"/g' settings.py.tmp >  settings.py.tmp2

sed 's/'$LIVE_URL'/'$LOCAL_URL'/g' settings.py.tmp2 >  settings.py.tmp3

mv settings.py.tmp3 settings.py
rm -f settings.py.tmp
rm -f settings.py.tmp2
