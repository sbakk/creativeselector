# -*- coding: utf-8 -*-
from django.conf.urls.defaults import *
from django.contrib import admin
from django.contrib.sitemaps import GenericSitemap
from creativeselector.models import *
from django.contrib.sitemaps import Sitemap
import settings
import creativeselector



#############################
# SITEMAPS
#############################
class StaticPagesSitemap(Sitemap): 
    changefreq = "yearly" 
    priority = 0.5 
    def items(self): 
      return ['/',
              '/miez',
              '/rolunk',
              '/faq',
              '/feltetelek',
              '/adatkezeles',
              '/kapcsolat',
              ] 
    def location(self, obj): 
      return obj

sitemaps = {
#    'mentors': GenericSitemap({'queryset': Mentor.objects.all(),}, priority=0.6,  changefreq='never'),
    'blogs': GenericSitemap({'queryset': BlogEntry.objects.all(), 'date_field': 'created',}, priority=0.6,  changefreq='hourly'),
    'tenders': GenericSitemap({'queryset': Tender.objects.all(), 'date_field': 'created',}, priority=0.6,  changefreq='hourly'),
    'statics': StaticPagesSitemap,
}

#############################
# ADMIN URLS
#############################
admin.autodiscover()

urlpatterns = patterns('',
  (r'^admin/', include(admin.site.urls)),
)

#############################
# SITEMAP URLS
#############################
urlpatterns += patterns('',
  (r'^sitemap\.xml$', 'django.contrib.sitemaps.views.sitemap', {'sitemaps': sitemaps}),
)

#############################
# FACEBOOK URLS
#############################
urlpatterns += patterns('',
    (r'^facebook/', include('django_facebook.urls')),
)

#############################
# VIEW URLS
#############################

if settings.MEDIA_DEV_MODE:
    # Generate media on-the-fly
    from mediagenerator.urls import urlpatterns as mediaurls
    urlpatterns += mediaurls


urlpatterns += patterns('creativeselector.views',
  (r'^upload/', include('adminupload.urls')),
  (r'^$', 'index'),
  (r'^import_rss$', 'import_rss'),
  (r'^switch_lang$', 'to_english'),
  (r'^projekts_are_late$', 'end_projekts'),
  (r'^unfinished_projs_email$', 'unfinished_projects'),
  #(r'^remove_tenders$', 'remove_tenders'),
  (r'^projekt_bekuldes$', 'send_project'),
  (r'^projektbekuldes2$', 'send_project2'),
  (r'^projektbekuldes3$', 'send_project3'),
  (r'^projekt_folyt/(.+)$', 'project_continue'),
  (r'^projekt_szerk/(.+)$', 'continue_project'),
  (r'^projekt_szerk2/(.+)$', 'continue_project2'),
  (r'^projekt_szerk3/(.+)$', 'continue_project2_2'),
  (r'^projekt_szerk4/(.+)$', 'continue_project3'),
  (r'^projekt_szerk5/(.+)$', 'continue_project4'),
  (r'^projekt_szerk6/(.+)$', 'continue_project5'),
  (r'^projekt_mentes/(.+)/(.+)$', 'saveproject'),
  (r'^projekt/show/(.+)$', 'get_project'),
  (r'^projekt/update/(.+)$', 'get_project_update'),
  (r'^projekt_aktivalas/(.+)$', 'projectpublic'),
  (r'^projekt_kiemeles/(.+)$', 'projecttomost'),
  (r'^projekt/supporters/(.+)$', 'projekt_supporters'),
  (r'^projekt/support/(.+)$', 'present_form'),
  (r'^pay/(.+)$', 'send_money'),
  (r'^pay2/(.+)/$', 'send_money2'),
  (r'^delete_trans/(.+)/(.+)/$', 'erase_transaction'),
  (r'^activate_proj/(.+)$', 'validate_project'),
  (r'^activate_proj_ok/(.+)$', 'validate_project_ok'),
  (r'^activate_proj_no/(.+)$', 'validate_project_no'),
  #(r'^reszletes_kiiras$', 'detailed_tender'),
  (r'^egyblog/(.+)$', 'blogentry'),
  #(r'^blogcomment/(.+)$', 'comment_show'),
  (r'^blogcommentdel/(.+)$', 'comment_del'),
  (r'^ideaorder/(.+)$', 'ideaorder'),
  (r'^blog/tag/(.+)$', 'blog_tag'),
  (r'^blog/oldal/(\d+)$', 'blog'),
  #(r'^remove_idea/(.+)$', 'remove_idea'),
  (r'^excel_export$', 'excel_export'),
  #(r'^excel_export_new$', 'excel_export_new'),
  #(r'^ideauser_get$', 'ideauser_get'),
  (r'^users/get/them/all/$', 'user_get'),
  #(r'^test_mail_send$', 'test_mail_send'),
  #(r'^gallery$', 'gallery'),
  (r'^comments/blog/list/$', 'list_all_comments'),
  (r'^proj/admin/list/$', 'admin_projs'),
  (r'^endproj/admin/list/$', 'admin_end_projs'),
  (r'^sups/admin/list/$', 'admin_sups'),
  (r'^budget/admin/list/$', 'admin_budget'),
  (r'^most/admin/list/$', 'mosttender_admin'),
  (r'^new_proj/admin/list/$', 'admin_new_proj'),
  (r'^names/admin/list/$', 'admin_names'),
  (r'^names/admin/to_ex/$', 'names_to_excel'),
  (r'^proj/admin/(.+)$', 'admin_list_proj'),
  (r'^user_reg$', 'userreg'),
  (r'^activate/(.+)$', 'activate_user'),
  (r'^forget_pass/(.+)$', 'forget_pass'),
  (r'^user_profil/(.+)$', 'get_user_profile'),
  (r'^own_projekt/(.+)$', 'owner_project'),
  (r'^user_log$', 'user_login'),
  (r'^unsubscribe$', 'unsubscribe'),
  (r'^main_pass$', 'passed_page'),
  (r'^user_log_out$', 'user_logout'),
  (r'^delete_user$', 'deluser'),
  (r'^delete_user_final$', 'deleteuser'),
  #(r'^osszes$', 'all_ideas'),
  (r'^export_users_batch/(\d+)/(\d+)/(\d+)$', 'export_users_batch'),
  
  (r'^export_users$', 'export_users'),
  (r'^ideasbytag/(.+)$', 'ideasbytag'),
  (r'^projektek/kiemelt/kat/(.+)$', 'ideasbymost'),
  (r'^projektek/kat/fin/(.+)$', 'ideasbyfin'),
  (r'^projektek/kat/success/(.+)$', 'ideasbyfin'),
  (r'^projektek/kat/(.+)$', 'ideasbyfresh'),
  (r'^projektek$', 'ideas'),
  (r'^frissprojektek$', 'ideasbyfresh'),
  (r'^finishprojektek$', 'ideasbyfin'),
  (r'^endprojektek$', 'ideasbyend'),
  (r'^sikeresprojektek$', 'ideasbysucces'),
  (r'^projektek/kiemelt/(.+)/(.+)/kat/(.+)$', 'mostideas'),
  (r'^projektek/kiemelt/(.+)/(.+)$', 'mostideas'),
  (r'^projektek/friss/(.+)/(.+)/kat/(.+)$', 'freshideas'),
  (r'^projektek/friss/(.+)/(.+)$', 'freshideas'),
  (r'^projektek/finish/(.+)/(.+)/kat/(.+)$', 'finideas'),
  (r'^projektek/finish/(.+)/(.+)$', 'finideas'),
  (r'^projektek/end/(.+)/(.+)/kat/(.+)$', 'endideas'),
  (r'^projektek/end/(.+)/(.+)$', 'endideas'),
  (r'^projektek/success/(.+)/(.+)/kat/(.+)$', 'succesideas'),
  (r'^projektek/success/(.+)/(.+)$', 'succesideas'),
    
  (r'^refresh_related_items$', 'refresh_related_items'),
  (r'^refresh_one_related_item/(.+)$', 'refresh_one_related_item'),
  
  
  (r'^export_users_task/(\d+)$', 'export_users_task'),
  #(r'^otlet_bekuldese/(.+)$', 'send_idea'),
  #(r'^otlet_bekuldese$', 'send_idea'),
  #(r'^support_idea$', 'support_idea'),
  #(r'^fajl_feltoltese/(.+)$', 'send_idea2'),
  (r'^profilom$', 'profile'),
  #(r'^avatar_torles$', 'delete_user_photo'),
  #(r'^flush_memcache$', 'flush_memcache'),
  (r'^remove/tenderimage/(.+)$', 'remove_tenderimage'),
  #(r'^update_users$', 'update_users'),
  #(r'^otlet/(.+)$', 'ideaentry'),

  #(r'^necc/list/(.+)$', 'necc_category'),
  #(r'^necc/list$', 'necc_category_all'),
  #(r'^necc/search/(.+)$', 'necc_search'),
  
  (r'^tagging_autocomplete/', include('tagging_autocomplete.urls')),

  #(r'^setsupport$', 'setsupport'),
  (r'^(.+)$', 'render_direct'),
)




